
import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import FTPopOverMenu
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        /// clear token from previous version 
        if !UserDefaults.standard.bool(forKey: "newVersion") {
            UserDefaults.standard.removeObject(forKey: "token")
            UserDefaults.standard.set(true, forKey: "newVersion")
            UserDefaults.standard.synchronize()
        }
        ///
        
        IQKeyboardManager.sharedManager().enable = true
        GMSServices.provideAPIKey("AIzaSyAkN1ZAqXBNhtLR5p6T3hEeAaVvvUX9dO8")
        UIApplication.shared.statusBarStyle = .default
        StyleManager.shared.styleAppearance()
        CacheManager.updateCache()
        FTPopOverMenuConfiguration.default().textFont = UIFont.systemFont(ofSize: 14)
        FTPopOverMenuConfiguration.default().menuWidth = 140
        Fabric.with([Crashlytics.self])
        return true
    }
}

