

import UIKit

class EmptyLabel: UILabel {

    dynamic var titleLabelFont: UIFont! {
        get { return self.font }
        set { self.font = newValue }
    }

}
