

import UIKit

public enum CatalogStyle: String {
    case text               = "TEXT"
    case verticalIcon       = "VERTICAL_ICON"
    case verticalImage      = "VERTICAL_IMAGE"
    case cellIcon           = "CELL_ICON"
    case cellImage          = "CELL_IMAGE_1"
    case cellImage2         = "CELL_IMAGE_2"
    
    init(style: String) {
        self = CatalogStyle(rawValue: style) ?? .text
    }
}

protocol CatalogViewDelegate {
    func didChangeHeight(in catalogItem: CatalogView, height: CGFloat)
    func catalogView(_ catalogView: CatalogView, didSelectItemAt index: Int)
}

class CatalogView: UIView {
    var catalog: CatalogModel?
    let cellsInRow: CGFloat = 2.0
    let observationKey = "catalogCollectionView.contentSize"
    var delegate: CatalogViewDelegate?
    var catalogCollectionView: UICollectionView!
    var defaultStyle: CatalogStyle {
        get {
            if catalog != nil {
                return catalog!.view
            } else {
                return .text
            }
        }
    }
    
    // MARK: Lifecycle
    func catalogLayout() -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout.init()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.sectionInset = .zero
        let insets = CGFloat((Int(cellsInRow) - 1) * 1)
        switch defaultStyle {
        case .text:
            layout.itemSize = CGSize(width: frame.width, height: 50)
            return layout
        case .verticalIcon:
            layout.itemSize = CGSize(width: frame.width, height: 60)
            return layout
        case .verticalImage:
            let width = frame.width
            layout.itemSize = CGSize(width: width, height: 120)
            return layout
        case .cellIcon:
            let width = ceil(Double((frame.width - insets)  / cellsInRow))
            layout.itemSize = CGSize(width: width, height: 100)
            return layout
        case .cellImage:
            let width = ceil(frame.width - insets) / cellsInRow
            layout.itemSize = CGSize(width: width, height: 150)
            return layout
        case .cellImage2:
            let width = (frame.width - insets) / cellsInRow
            layout.itemSize = CGSize(width: width, height: 100)
            return layout
        }
    }
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }
    
    // MARK: Methods
    
    private func setup() {
        catalogCollectionView = UICollectionView.init(frame: bounds , collectionViewLayout: UICollectionViewFlowLayout())
        catalogCollectionView.showsHorizontalScrollIndicator = false
        catalogCollectionView.showsVerticalScrollIndicator = false
        catalogCollectionView.contentInset = .zero
        catalogCollectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(catalogCollectionView)
        catalogCollectionView.backgroundColor = UIColor.groupTableViewBackground
        catalogCollectionView.delegate = self
        catalogCollectionView.dataSource = self
        layoutCatalogCollectionView()
        registerCatalogCells()
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
    }
    
    private func layoutCatalogCollectionView() {
        catalogCollectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        catalogCollectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        catalogCollectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        catalogCollectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    private func registerCatalogCells() {
        catalogCollectionView.register(CatalogCollectionViewCellText.nib(), forCellWithReuseIdentifier: CatalogStyle.text.rawValue)
        catalogCollectionView.register(CatalogCollectionViewCellVerticalIcon.nib(), forCellWithReuseIdentifier: CatalogStyle.verticalIcon.rawValue)
        catalogCollectionView.register(CatalogCollectionViewCellVerticalImage.nib(), forCellWithReuseIdentifier: CatalogStyle.verticalImage.rawValue)
        catalogCollectionView.register(CatalogCollectionViewCellIcon.nib(), forCellWithReuseIdentifier: CatalogStyle.cellIcon.rawValue)
        catalogCollectionView.register(CatalogCollectionViewCellImage.nib(), forCellWithReuseIdentifier: CatalogStyle.cellImage.rawValue)
        catalogCollectionView.register(CatalogCollectionViewCellImage.nib(), forCellWithReuseIdentifier: CatalogStyle.cellImage2.rawValue)
    }
    
    func configure(for catalog: CatalogModel) {
        self.catalog = catalog
        reloadData()
    }
    
    func reloadData() {
        catalogCollectionView.reloadData()
    }
    
    // MARK: Observation
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            if delegate != nil {
                var height = catalogCollectionView.collectionViewLayout.collectionViewContentSize.height
                if (catalogCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection == .horizontal {
                    height = (catalogCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize.height
                }
                delegate?.didChangeHeight(in: self, height: height)
            }
        }
    }
}

extension CatalogView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.delegate != nil {
            self.delegate!.catalogView(self, didSelectItemAt: indexPath.item)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if catalog != nil  {
            return catalog!.items.count
        }
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: defaultStyle.rawValue, for: indexPath) as! CatalogCollectionViewCell
        cell.configure(for: catalog!.items[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let insets = CGFloat((Int(cellsInRow) - 1) * 1)
        switch defaultStyle {
        case .text:
            return CGSize(width: frame.width, height: 50)
        case .verticalIcon:
            return CGSize(width: frame.width, height: 60)
        case .verticalImage:
            let width = frame.width
            let currentItem = catalog!.items[indexPath.item]
            if let image = currentItem.image {
                let heigth = width * (CGFloat(image.height) / CGFloat(image.width))
                return CGSize(width: width, height: heigth)
            }
            return CGSize(width: width, height: 120)
        case .cellIcon:
            let width = ceil(Double((frame.width - insets)  / cellsInRow))
            return CGSize(width: width, height: 100)
        case .cellImage:
            let width = ceil(frame.width - insets) / cellsInRow
            return CGSize(width: width, height: 150)
        case .cellImage2:
            let width = (frame.width - insets) / cellsInRow
            let currentItem = catalog!.items[indexPath.item]
            if let image = currentItem.image {
                let heigth = width * (CGFloat(image.height) / CGFloat(image.width))
                return CGSize(width: width, height: heigth)
            }
            return CGSize(width: width, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}
