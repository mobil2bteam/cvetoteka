
import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var filterNameLabel: UILabel!
    @IBOutlet weak var filterValuesLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    func configure(for filter: FormattedFilterModel) {
        countLabel.text = "(\(filter.filter.count))"
        filterNameLabel.text = filter.name
        filterValuesLabel.text = filter.formattedValue()
   
        var temp = [String]()
        for f in filter.filter {
            if f.checked {
                temp.append(f.name)
            }
        }
        if temp.count == 0 || temp.count == filter.filter.count{
            filterValuesLabel.text = "Все значения"
            filterNameLabel.font = UIFont.systemFont(ofSize: 13)
        } else {
            filterValuesLabel.text = temp.joined(separator: ", ")
            filterNameLabel.font = UIFont.boldSystemFont(ofSize: 13)
        }
    }
    
}
