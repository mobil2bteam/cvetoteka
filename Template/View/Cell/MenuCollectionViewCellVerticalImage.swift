
import UIKit
import AlamofireImage

class MenuCollectionViewCellVerticalImage: MenuCollectionViewCell {

    @IBOutlet var textLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    override func configure(for menuItem: MenuItemModel) {
        textLabel.text = menuItem.name
        textLabel.textColor = UIColor(menuItem.textColor)
        textLabel.isHidden = true
        guard let image = menuItem.image,
            let url =  URL(string: image.url) else {
                imageView.image = nil
                return
        }
        imageView.af_setImage(withURL: url)
    }

}
