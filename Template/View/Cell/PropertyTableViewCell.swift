

import UIKit

class PropertyTableViewCell: UITableViewCell {


    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var dotView: UIView!
    
    var addToCartHandler: ((PriceModel) -> ())?
    var price: PriceModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cartView.backgroundColor = UIColor.colorPrimary
    }
    
    func configure(for price: PriceModel) {
        priceLabel.text = "\(price.price * price.minCount!) p."
        countLabel.text = "от \(price.minCount!) шт. \(price.price) р."
        self.price = price
    }
    
    func setDotActive(active: Bool) {
        self.dotView.backgroundColor = active == true ? UIColor.colorAccent : UIColor.white
    }
    
    @IBAction func addToCartButtonClicked(_ sender: Any) {
        addToCartHandler?(price)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        priceLabel.text = ""
        countLabel.text = ""
        setDotActive(active: false)
    }
}
