
import UIKit
import AlamofireImage

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var countLabel: MainLabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: MainLabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!

    var item: CatalogModel?
    var deleteHandler: ((CatalogModel) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        saleLabel.backgroundColor = UIColor.colorBackgroundFlagDiscount
        saleLabel.textColor = UIColor.colorTextFlagDiscount
        deleteButton.tintColor = UIColor.colorTextPrimary
    }
    
    func configure(for item: CatalogModel) {
        self.item = item
        nameLabel.text = item.name
        countLabel.text = "\(CartManager.count(for: item.prices.first?.id ?? 0))"
        if item.salePercent > 0 {
            let text = "\(item.prices.first!.oldPrice!)"
            let attrString = NSAttributedString(string: text, attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
            oldPriceLabel.attributedText = attrString
            if item.prices.first!.oldPrice == 0 {
                oldPriceLabel.text = ""
            }
            priceLabel.textColor = UIColor.colorTextDiscountPrice
            priceLabel.text = "\(item.prices[0].price) р."
            oldPriceLabel.textColor = UIColor.colorTextOldPrice
        } else {
            let price = item.prices[0]
            priceLabel.text = "\(price.price) р."
            oldPriceLabel.text = ""
            priceLabel.textColor = UIColor.colorTextPrice
        }
        if SettingsManager.showSaleOnProducts {
            if item.salePercent > 0 {
                saleLabel.isHidden = false
                saleLabel.text = "\(item.salePercent)%"
            }
        }
        guard let image = item.image,
            let url =  URL(string: image.url) else {
                productImageView.image = nil
                return
        }
        productImageView.af_setImage(withURL: url)
    }
    
    @IBAction func deleteButtonClicked(_ sender: Any) {
        deleteHandler!(item!)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        saleLabel.isHidden = true
        priceLabel.text = ""
        oldPriceLabel.text = ""
    }
    
}
