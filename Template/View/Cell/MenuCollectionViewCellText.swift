
import UIKit

class MenuCollectionViewCellText: MenuCollectionViewCell {

    @IBOutlet var textLabel: UILabel!
    
    override func configure(for menuItem: MenuItemModel) {
        //backgroundColor = UIColor(menuItem.backgroundColor)
        textLabel.textColor = UIColor(menuItem.textColor)
        textLabel.text = menuItem.name
    }

}
