
import UIKit

class CatalogCollectionViewCellText: CatalogCollectionViewCell {

    @IBOutlet var textLabel: UILabel!

    override func configure(for catalogItem: CatalogModel) {
        textLabel.text = catalogItem.notShowName == true ? "" : catalogItem.name
    }
}
