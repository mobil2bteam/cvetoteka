

import UIKit
import AlamofireImage

class CatalogCollectionViewCellVerticalImage: CatalogCollectionViewCell {

    @IBOutlet var textLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    override func configure(for catalogItem: CatalogModel) {
        textLabel.text = catalogItem.notShowName == true ? "" : catalogItem.name
        
        guard let image = catalogItem.image?.url,
            let url =  URL(string: image) else {
                imageView.image = nil
                return
        }
        imageView.af_setImage(withURL: url)
    }

}
