
import UIKit
import AlamofireImage

class CatalogCollectionViewCellVerticalIcon: CatalogCollectionViewCell {

    @IBOutlet var textLabel: UILabel!
    @IBOutlet var imageView: UIImageView!

    override func configure(for catalogItem: CatalogModel) {
        textLabel.text = catalogItem.notShowName == true ? "" : catalogItem.name
        guard let image = catalogItem.icon.url,
            let url =  URL(string: image) else {
                imageView.image = nil
                return
        }
        imageView.af_setImage(withURL: url)
    }

}
