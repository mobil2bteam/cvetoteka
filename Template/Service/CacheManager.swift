
import Foundation
import RealmSwift

class CacheManager: NSObject {
    
    /**
     Remove cache which is expired.
     */
    class func updateCache() {
        for cache in uiRealm.objects(CacheRealmModel.self) {
            if cache.expiryDate().compare(Date()) == .orderedAscending {
                try! uiRealm.write{
                    uiRealm.delete(cache)
                }
            }
        }
    }
    
    /**
     Clear all cache.
     */
    class func clearAllCache() {
        try! uiRealm.write{
            uiRealm.delete(uiRealm.objects(CacheRealmModel.self))
        }
    }
    
    /**
     Add cache
     - parameter cache: Cache need to be added.
     */
    class func addCache(cache: CacheRealmModel) {
        // clear exist cache for this response in order to have always actual cache
        for c in uiRealm.objects(CacheRealmModel.self) {
            if c.parameters == cache.parameters && c.url == cache.url {
                try! uiRealm.write{
                    uiRealm.delete(c)
                }
            }
        }
        try! uiRealm.write{
            uiRealm.add(cache)
        }
    }
    
    /**
     Find and get cache for url request.
     - parameter url: Url for request.
     - parameter parameters: Parameters for request, can be nil.
     - returns: cache if it was found, otherwise nil.
     */
    class func cache(for url: String, parameters: [String: String]? = nil) -> CacheRealmModel? {
        let cacheResults = uiRealm.objects(CacheRealmModel.self)
        for result in cacheResults {
            if result.url == url {
                if parameters == nil  {
                    return result
                } else {
                    if  let parametersString = parameters!.convertToString(), result.parameters! == parametersString {
                        return result
                    }
                }
            }
        }
        return nil
    }
}

class CacheRealmModel: Object{
    dynamic var parameters: String? = nil
    dynamic var url: String = ""
    dynamic var seconds: Int = 0
    dynamic var date: Date = Date()
    dynamic var response: String = ""
    
    convenience init(url: String, response: Dictionary<String, Any>, seconds: Int, parameters: Dictionary<String, String>? = nil) {
        self.init()
        self.url = url
        if let responseString = response.convertToString() {
            self.response = responseString
        }
        if parameters != nil, let parametersString = parameters!.convertToString() {
            self.parameters = parametersString
        }
        self.seconds = seconds
        self.date = Date()
    }
    
    
    /**
     Get expiry date.
     - returns: expiry date for cache
     */
    func expiryDate() -> Date {
        return date.addingTimeInterval(TimeInterval(seconds))
    }
}

