
import Foundation
import UIKit

class Router {
    
    class func productViewController(productId: Int) -> UIViewController {
        let vc = ProductViewController(productId: productId)
        return vc
    }
 
    class func productsViewController (for params: [String: String]) -> UIViewController {
        let vc = getProductsViewController()
        vc.params = params
        return vc
    }
    
    class func productsViewController (for catalogId: Int) -> UIViewController {
        let vc = getProductsViewController()
        let params = ["catalog_id": "\(catalogId)"]
        vc.params = params
        return vc
    }
    
    class func catalogViewController (catalog: CatalogModel) -> UIViewController {
        let vc = UIStoryboard.init(name: "Catalog", bundle: nil).instantiateViewController(withIdentifier: "CatalogViewController") as! CatalogViewController
        vc.catalog = catalog
        return vc
    }

    class func profileViewController() -> UIViewController {
        let vc = UIStoryboard.init(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController")
        return vc
    }
    
    class func logInViewController() -> LogInViewController {
        let vc = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        return vc
    }

    class func signUpViewController() -> SignUpViewController {
        let vc = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        return vc
    }
    
    class func promotionViewController(for promoId: Int) -> PromotionViewController {
        let vc = UIStoryboard.init(name: "Promotions", bundle: nil).instantiateViewController(withIdentifier: "PromotionViewController") as! PromotionViewController
        vc.newsId = promoId
        return vc
    }
    
    class func menuInfoViewController(for menuId: Int) -> MenuInfoViewController {
        let vc = UIStoryboard.init(name: "Info", bundle: nil).instantiateViewController(withIdentifier: "MenuInfoViewController") as! MenuInfoViewController
        vc.menuId = menuId
        return vc
    }

    class func pointListViewController(for points: [PointModel]) -> PointListViewController {
        let vc = PointListViewController.init(points: points)
        return vc
    }

    class func workingViewController(for working: [WorkModel]) -> WorkingViewController {
        let vc = UIStoryboard.init(name: "Info", bundle: nil).instantiateViewController(withIdentifier: "WorkingViewController") as! WorkingViewController
        vc.working = working
        return vc
    }
    
    class func descriptionViewController(for description: String) -> DescriptionViewController {
        let vc = UIStoryboard.init(name: "Info", bundle: nil).instantiateViewController(withIdentifier: "DescriptionViewController") as! DescriptionViewController
        vc.descriptionText = description
        return vc
    }

    class func orderDeliveryViewController(for cart: CartModel, params: [String: String]) -> OrderDeliveryViewController {
        let vc = UIStoryboard.init(name: "OrderDelivery", bundle: nil).instantiateInitialViewController() as! OrderDeliveryViewController
        vc.cart = cart
        vc.params = params
        return vc
    }
    
    class func orderDetailViewController(for delivery: DeliveryModel) -> OrderDetailViewController {
        let vc = UIStoryboard.init(name: "OrderDetail", bundle: nil).instantiateInitialViewController() as! OrderDetailViewController
        vc.delivery = delivery
        return vc
    }
    
    class func orderAddAddressViewController() -> OrderAddAddressViewController {
        let vc = UIStoryboard.init(name: "OrderAddAddress", bundle: nil).instantiateInitialViewController() as! OrderAddAddressViewController
        return vc
    }
    
    class func orderAddRecipientViewController() -> OrderAddRecipientViewController {
        let vc = UIStoryboard.init(name: "OrderAddRecipient", bundle: nil).instantiateInitialViewController() as! OrderAddRecipientViewController
        return vc
    }

    class func orderAddCommentViewController() -> OrderAddCommentViewController {
        let vc = UIStoryboard.init(name: "OrderAddComment", bundle: nil).instantiateInitialViewController() as! OrderAddCommentViewController
        return vc
    }

    class func orderAddDateViewController() -> OrderAddDateViewController {
        let vc = UIStoryboard.init(name: "OrderAddDate", bundle: nil).instantiateInitialViewController() as! OrderAddDateViewController
        return vc
    }

    class func orderSubmitViewController() -> OrderSubmitViewController {
        let vc = UIStoryboard.init(name: "OrderSubmit", bundle: nil).instantiateInitialViewController() as! OrderSubmitViewController
        return vc
    }

    // MARK: Private
    class func getProductsViewController() -> ProductListViewController {
        let vc = UIStoryboard.init(name: "Catalog", bundle: nil).instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        return vc
    }
    
}
