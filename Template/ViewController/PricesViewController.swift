
import UIKit

class PricesViewController: UIViewController {

    @IBOutlet weak var pricePickerView: UIPickerView!
    @IBOutlet weak var priceLabel: SecondLabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    var completionHandler: (() -> ())?
    var product: ProductModel!
    var count: Int = 0 {
        didSet {
            if let price = product.priceFor(count: count) {
                priceLabel.text = "1 роза = \(price) p."
                totalPriceLabel.text = "\(count * price) p."
            } else {
                priceLabel.text = ""
                totalPriceLabel.text = ""
            }
        }
    }
    
    convenience init(product: ProductModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.product = product
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if product.allValues.count > 0 {
            count = product.allValues.first!
        }
    }

    @IBAction func cartButtonClicked(_ sender: Any) {
        CartManager.deleteProducts(productId: product.id)
        if let priceId = product.priceIdFor(count: count) {
            CartManager.addProduct(for: priceId, productId: product.id, count: count)
        }
        completionHandler?()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension PricesViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return product.allValues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(product.allValues[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        count = product.allValues[row]
    }
}
