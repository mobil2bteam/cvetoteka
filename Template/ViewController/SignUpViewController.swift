
import UIKit
import SVProgressHUD

typealias SignInUserBlock = () -> ()

class SignUpViewController: UIViewController {

    
    // MARK: Outlets
    @IBOutlet weak var passwordTextField: TextField!
    @IBOutlet weak var lastNameTextField: TextField!
    @IBOutlet weak var nameTextField: TextField!
    @IBOutlet weak var emailTextField: TextField!
    @IBOutlet weak var passwordView: UIView!

    
    // MARK: Properties
    var completionBlock: SignInUserBlock?

    
    // MARK: Actions
    @IBAction func cancelButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        let vc = Router.logInViewController()
        vc.completionBlock = completionBlock
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUpButtonClicked(_ sender: Any) {
        view.endEditing(true)
        if lastNameTextField.text!.isEmpty {
            lastNameTextField.shake()
            return
        }
        if nameTextField.text!.isEmpty {
            nameTextField.shake()
            return
        }
        if emailTextField.text!.isEmpty {
            emailTextField.shake()
            return
        }
        if passwordTextField.text!.isEmpty {
            passwordTextField.shake()
            return
        }
        let params = ["first_name": nameTextField.text!,
                      "user_name": nameTextField.text!,
                      "second_name": lastNameTextField.text!,
                      "password": passwordTextField.text!,
                      "login": emailTextField.text!,
                      "email": emailTextField.text!]
        SVProgressHUD.show()
        UserManager.registerUser(with: params) { [weak self] (user, error) in
            SVProgressHUD.dismiss()
            if user != nil && self?.completionBlock != nil {
                self?.completionBlock!()
            }
            if error != nil {
                self?.alert(message: error!)
            }
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == lastNameTextField {
            _ = nameTextField.becomeFirstResponder()
        }
        if textField == nameTextField {
            _ = emailTextField.becomeFirstResponder()
        }
        if textField == emailTextField {
            _ = passwordTextField.becomeFirstResponder()
        }
        _ = textField.resignFirstResponder()
        return true
    }
    
}

