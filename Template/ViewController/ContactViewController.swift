
import UIKit
import AlamofireImage
import ImageSlideshow

class ContactViewController: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var clubLabel: UILabel!
    @IBOutlet weak var clubImageView: UIImageView!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var officeView: UIView!
    
    var descriptionSegue = "descriptionSegue"
    var workSegue = "workSegue"
    var company = AppManager.shared.options.settings.company!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Адрес и контакты"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    func prepareView() {
        buttons.forEach{ $0.setTitleColor(UIColor.colorTextPrimary, for: .normal) }
        clubLabel.text = company.name
        addressLabel.text = company.contact.address.street
        prepareSliderView()
        officeView.isHidden = company.pointsCities().count == 0
    }

    func prepareSliderView() {
        if let slider = company.slider {
            if slider.items.count > 0 {
                // Add aspect ratio to slider
                let multiplier = slider.items[0].image.aspectRatio
                slideshow.heightAnchor.constraint(equalTo: slideshow.widthAnchor, multiplier: multiplier).isActive = true
                DispatchQueue.main.async { [weak self] in
                    self?.view.layoutIfNeeded()
                }
            }
            if slider.auto {
                slideshow.slideshowInterval = Double(slider.speed / 1000)
            } else {
                slideshow.slideshowInterval = 0
            }
            slideshow.draggingEnabled = slider.manual
            slideshow.pageControlPosition = slider.items.count > 1 ? PageControlPosition.insideScrollView : PageControlPosition.hidden
            slideshow.pageControl.currentPageIndicatorTintColor = UIColor.colorAccent
            slideshow.pageControl.pageIndicatorTintColor = UIColor.white
            slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
            slideshow.setImageInputs(slider.items.map({ (item) -> AlamofireSource in
                return AlamofireSource(urlString: item.image.url)!
            }))
            slideshow.isHidden = false
        } else {
            slideshow.isHidden = true
        }
    }
    
    // MARK: Actions
    @IBAction func callButtonClicked(_ sender: Any) {
        let onlyDigits = (company.contact.phone.components(separatedBy: CharacterSet.decimalDigits.inverted)).joined(separator: "")
        let phoneURL = URL.init(string: "telprompt:\(onlyDigits)")
        if phoneURL != nil {
            openURL(urlString: "telprompt:\(onlyDigits)")
        }
    }
    
    @IBAction func pointsButtonClicked(_ sender: Any) {
        let pointCities = company.pointsCities()
        if pointCities.count > 1 {
            let vc = PointsCityListViewController.init()
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let points = company.points(for: pointCities.first!)
            let vc = Router.pointListViewController(for: points)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func siteButtonClicked(_ sender: Any) {
        if let site = company.social?.web {
            openURL(urlString: site)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == descriptionSegue {
            let vc = segue.destination as! DescriptionViewController
            vc.descriptionText = company.descriptionLong
        }
        if segue.identifier == workSegue {
            let vc = segue.destination as! WorkingViewController
            vc.working = company.workingHours
        }
    }

    //MARK: Helper
    func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

}
