
import UIKit
import SVProgressHUD

class OrderSubmitViewController: UIViewController {

    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet weak var productsCollectionView: UICollectionView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    
    var delivery: DeliveryModel!
    var params: [String: String]!
    var address: AddressDataModel?
    var recipient: RecipientDataModel?
    var comment: String = ""
    var date: Date?
    var addressPoint: PointModel?
    
    var selectedPayment: PaymentModel? {
        didSet {
            paymentTableView.reloadData()
        }
    }
    var order: OrderModel! {
        didSet {
            prepareView()
        }
    }
    
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Новый заказ"
        paymentTableView.tableFooterView = UIView.init()
        selectedPayment = delivery.payments.first
        makeOrder(preOrder: true)
    }
    
    
    // MARK: Methods
    func makeOrder(preOrder: Bool) {
        params["deliveryId"] = "\(delivery!.id!)"
        if addressPoint != nil {
            params["addressPoint"] = "\(addressPoint!.id!)"
        }
        if address != nil {
            params["address"] = "\(address!.formattedAddress())"
            params["zipCode"] = address!.zipCode
        }
        if date != nil {
            let dateFormatter = DateFormatter(withFormat: "d.MM.y", locale: "ru_RU")
            params["date"] = dateFormatter.string(from: date!)
        }
        params["paymentId"] = "\(selectedPayment?.id ?? 0)"
        if recipient != nil {
            params["contact_phone"] = "\(recipient!.phone)"
            params["contact_name"] = "\(recipient!.name)"
        }
        params["comment"] = comment
        if preOrder {
            params["pre"] = "1"
        } else {
            params["pre"] = "0"
        }
        for (key, value) in CartManager.cartParams() {
            params[key] = value
        }
        SVProgressHUD.show()
        OptionsManager.order(params: params) { [weak self] (order, error) in
            SVProgressHUD.dismiss()
            if error != nil {
                self?.errorLabel.text = error!
            }
            if order != nil {
                if !preOrder {
                    self?.order = order
                    self?.performSegue(withIdentifier: "finishOrderSegue", sender: nil)
                } else {
                    self?.order = order
                }
            }
        }
    }

    func prepareView() {
        saleLabel.text = "\(order.totalSumSale) p."
        summLabel.text = "\(order.totalNotSale) p."
        totalLabel.text = "\(order.total) p."
        deliveryLabel.text = "\(order.sumDelivery) p."
        productsCollectionView.reloadData()
        errorView.isHidden = true
    }
    
    
    // MARK: Actions
    @IBAction func orderButtonClicked(_ sender: Any) {
        makeOrder(preOrder: false)
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "finishOrderSegue" {
            let vc = segue.destination as! OrderFinishViewController
            vc.order = order
        }
    }
}

extension OrderSubmitViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if delivery != nil {
            return delivery.payments.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.tintColor = UIColor.colorPrimary
        let payment = delivery.payments[indexPath.row]
        if selectedPayment != nil {
            if payment.id == selectedPayment!.id {
                cell.accessoryType = .checkmark
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15)

            } else {
                cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
                cell.accessoryType = .none
            }
        } else {
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.accessoryType = .none
        }
        cell.textLabel?.text = payment.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let payment = delivery.payments[indexPath.row]
        selectedPayment = payment
    }
}

extension OrderSubmitViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if order != nil {
            return order!.products.count
        }
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        if let image = order.products[indexPath.item].image {
            cell.configure(for: image.url)
        }
        return cell
    }
    
}

