
import UIKit

class DescriptionViewController: UIViewController {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    var descriptionText: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Описание"
        descriptionLabel.setHtmlText(descriptionText)
    }
    
}
