
import UIKit

class PointsCityListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let identifier = String(describing: CityTableViewCell.self)
    let searchController = UISearchController(searchResultsController: nil)
    var pointsCities = AppManager.shared.options.settings.company.pointsCities()
    
    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupSearchController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Город"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView.init()
        tableView.register(CityTableViewCell.nib(), forCellReuseIdentifier: identifier)
    }
    
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск города"
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
    }
    
    // MARK: Actions
    
    func dissmiss() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Helper
    
    func cityArray() -> [String] {
        let searchText = searchController.searchBar.text
        if (searchText!.characters.count > 0) {
            return pointsCities.filter {
                $0.range(of: searchText!, options: .caseInsensitive) != nil
            }
        } else {
            return pointsCities
        }
    }
}

extension PointsCityListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityArray().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CityTableViewCell
        cell.cityLabel.text = cityArray()[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let points = AppManager.shared.options.settings.company.points(for: cityArray()[indexPath.row])
        let vc = Router.pointListViewController(for: points)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension PointsCityListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        tableView.reloadData()
    }
}
