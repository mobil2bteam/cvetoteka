
import UIKit

class FavoriteViewController: UIViewController {

    @IBOutlet weak var itemsCollectionView: UICollectionView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: EmptyLabel!
    
    let itemIdentifier = String(describing: ItemCollectionViewCell.self)
    var products: [CatalogModel]!
    
    var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(refreshProducts(sender:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Избранное"
        prepareEmptyView()
        itemsCollectionView.backgroundColor = UIColor.colorBackgroundDark
        itemsCollectionView.register(ItemCollectionViewCell.nib(), forCellWithReuseIdentifier: itemIdentifier)
        itemsCollectionView.addSubview(refreshControl)
        if #available(iOS 10, *) {
            itemsCollectionView.refreshControl = refreshControl
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if products == nil {
            loadFavoriteProducts()
        }
    }
    
    // MARK: Methods
    func loadFavoriteProducts() {
        let favoriteProducts = DataManager.getFavoriteProducts()
        self.refreshControl.beginRefreshing()
        if favoriteProducts.count > 0 {
            itemsCollectionView.setContentOffset(CGPoint(x:0 , y:itemsCollectionView.contentOffset.y - refreshControl.frame.size.height), animated: true)
            let stringId = favoriteProducts.map{String($0)}.joined(separator: "-")
            let params = ["cart": stringId]
            ProductManager.fetchProducts(with: params, successHandler: { (products, error) in
                self.refreshControl.endRefreshing()
                if products != nil {
                    self.products = products!
                    self.itemsCollectionView.reloadData()
                    self.emptyView.isHidden = products!.count != 0
                }
                if error != nil {
                    self.alert(message: error!)
                }
            })
        } else {
            self.products = nil
            self.refreshControl.endRefreshing()
            self.itemsCollectionView.reloadData()
            emptyView.isHidden = false
        }
    }
    
    func refreshProducts(sender: AnyObject) {
        loadFavoriteProducts()
    }
    
    func prepareEmptyView() {
        emptyLabel.setHtmlText(Constants.emptyFavoriteProducts)
    }


    //MARK: Navigation
    func showProductViewController(for productId: Int) {
        let vc = Router.productViewController(productId: productId)
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension FavoriteViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemIdentifier, for: indexPath) as! ItemCollectionViewCell
        cell.configure(for: products[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = products[indexPath.item]
        showProductViewController(for: product.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == itemsCollectionView {
            let width = collectionView.frame.size.width / 2.0 - 0.5
            return CGSize(width: width, height: ItemCollectionViewCell.itemHeight)
        } else {
            return (collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize
        }
    }
}

