
import UIKit
import SVProgressHUD

class CartViewController: UIViewController {

    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var cartTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var promoTextField: TextField!
    @IBOutlet weak var cartView: UIStackView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: EmptyLabel!
    @IBOutlet weak var ballSlider: UISlider!
    @IBOutlet weak var ballView: UIView!
    @IBOutlet weak var ballLabel: UILabel!

    let cartCellHeight = 120
    var user = AppManager.shared.currentUser
    var cart: CartModel! {
        didSet {
            prepareView()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareEmptyView()
        ballSlider.tintColor = UIColor.colorAccent
        ballLabel.textColor = UIColor.colorTextPrice
        cartView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(CartViewController.updateProductCount), name: CartManager.notificationName, object: nil)
        title = "Корзина"
        prepareBallView()
        loadCart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !CartManager.isEmpty {
            if cart != nil {
                cartView.isHidden = cart.products.count == 0
            }
        } else {
            emptyView.isHidden = false
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: CartManager.notificationName, object: nil)
    }

    // MARK: Methods
    func prepareEmptyView() {
        emptyLabel.setHtmlText(Constants.emptyBag)
    }
    
    func prepareView() {
        if cart.products.count > 0 {
            reloadTableView()
            cartView.isHidden = cart.products.count == 0
            saleLabel.text = "\(cart.totalSumSale) p."
            summLabel.text = "\(cart.totalNotSale) p."
            totalLabel.text = "\(cart.total) p."
            if promoTextField.text!.isEmpty {
                promoCodeLabel.text = ""
            } else {
                promoCodeLabel.text = "Промо-код не существует или просрочен"
                if let promoCode = cart.promoCode {
                    if promoCode.percent > 0 {
                        promoCodeLabel.text = "Данный промо-код дарит вам скидку на \(promoCode.percent)%"
                    }
                }
            }
            emptyView.isHidden = true
        } else {
            emptyView.isHidden = false
        }
        cartView.isHidden = false
    }
    
    func prepareBallView() {
        if (user?.balls ?? 0) > 0 {
            let ball = user!.balls
            ballSlider.maximumValue = Float(ball)
            ballView.isHidden = false
            ballLabel.text = "0/\(ball) ball"
        } else {
            ballView.isHidden = true
        }
    }
    
    func reloadTableView() {
        cartTableView.reloadData()
        cartTableViewHeightConstraint.constant = CGFloat(cartCellHeight * cart.products.count)
        view.layoutIfNeeded()
    }
    
    func updateProductCount() {
        if CartManager.isEmpty {
            emptyView.isHidden = false
        } else {
            if CartManager.productsCount() == cart.products.count {
                reloadTableView()
            } else {
                loadCart()
            }
        }
    }

    // MARK: Methods
    func loadCart(completion: (() -> ())? = nil) {
        if CartManager.productsCount() > 0 {
            SVProgressHUD.show()
            let params = cartParameters()
            OptionsManager.loadCart(params: params, successHandler: { (cart, error) in
                SVProgressHUD.dismiss()
                if cart != nil {
                    self.cart = cart
                    if completion != nil {
                        completion!()
                    }
                }
                if error != nil {
                    self.alert(message: error!.localizedDescription)
                }
            })
        } else {
            emptyView.isHidden = false
        }
    }

    func cartParameters() -> [String: String] {
        var params = CartManager.cartParams()
        if promoTextField.text!.characters.count > 0 {
            params["promocode"] = promoTextField.text!
        }
        if let card = user?.ballCard {
            params["cardId"] = "\(card.id!)"
        }
        if Int(ballSlider.value) > 0 {
            params["ball"] = "\(Int(ballSlider.value))"
        }
        return params
    }
    
    func showProductViewController(for productId: Int) {
        let vc = Router.productViewController(productId: productId)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Actions
    @IBAction func updateCartButtonClicked(_ sender: Any) {
        loadCart()
    }

    @IBAction func promoButtonClicked(_ sender: Any) {
        loadCart()
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        let value = Int(ballSlider.value)
        ballLabel.text = "\(value)/\(user?.balls ?? 0) ball"
    }
    
    @IBAction func sliderEndDragging(_ sender: Any) {
        loadCart()
    }

    @IBAction func orderButtonClicked(_ sender: Any) {
        let comletion = {
            let vc = Router.orderDeliveryViewController(for: self.cart, params: self.cartParameters())
            let navVC = UINavigationController.init(rootViewController: vc)
            self.present(navVC, animated: true, completion: nil)
        }
        loadCart(completion: comletion)
    }
}

extension CartViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cart != nil {
            return cart.products.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CartTableViewCell
        if SettingsManager.shoppingCartVariant1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cart1TableViewCell", for: indexPath) as! Cart1TableViewCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cart2TableViewCell", for: indexPath) as! Cart2TableViewCell
        }
        cell.configure(for: cart.products[indexPath.row])
        cell.deleteHandler = { [weak self] product in
            guard let `self` = self else { return }
            self.customAlert(title: "Вы уверены, что хотите удалить товар?", okHandler: {
                CartManager.deleteProducts(productId: product.id)
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !SettingsManager.shoppingCartVariant1 {
            showProductViewController(for: cart.products[indexPath.row].id)
        }
    }
}

