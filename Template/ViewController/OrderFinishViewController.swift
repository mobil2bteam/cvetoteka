
import UIKit
import SVProgressHUD

class OrderFinishViewController: UIViewController {

    var order: OrderModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.colorPrimary
        CartManager.clearCart()
    }


    @IBAction func okButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateOder() {
        SVProgressHUD.show()
        OptionsManager.updateOrder(for: order.id, isPayment: true) { (order, error) in
            SVProgressHUD.dismiss()
            if order != nil {
                if order!.isPayment {
                    self.alert(message: "Заказ оплачен")
                }
            }
            if error != nil {
                self.alert(message: "Произошла ошибка", title: "", handler: { action in
                    self.updateOder()
                })
            }
        }
    }
}
