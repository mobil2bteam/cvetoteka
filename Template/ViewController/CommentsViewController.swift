
import UIKit
import SVProgressHUD
import Presentr

class CommentsViewController: UIViewController {

    @IBOutlet weak var projectNameLabel: MainLabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var addCommentView: UIView!
    @IBOutlet weak var warningView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: EmptyLabel!
    
    var comments: CommentsModel!
    let presenter: Presentr = {
        let customPresenter = Presentr(presentationType: .fullScreen)
        customPresenter.transitionType = .coverVertical
        customPresenter.dismissTransitionType = .coverVertical
        customPresenter.roundCorners = false
        customPresenter.backgroundColor = .black
        customPresenter.backgroundOpacity = 0.5
        customPresenter.dismissOnSwipe = true
        customPresenter.dismissOnTap = true
        return customPresenter
    }()

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareEmptyView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if comments == nil {
            SVProgressHUD.show()
            CommentsManager.getComments { (comments, error) in
                if error != nil {
                    self.errorLabel.text = error!
                }
                if comments != nil {
                    self.comments = comments
                    self.prepareView()
                    self.errorView.isHidden = true
                    self.emptyView.isHidden = self.allComments().count != 0
                }
                SVProgressHUD.dismiss()
            }
        }
    }
    
    
    // MARK: Methods
    func prepareEmptyView() {
        emptyLabel.setHtmlText(Constants.emptyComment)
    }
    
    func prepareView() {
        if comments != nil {
            projectNameLabel.text = comments.name
            ratingLabel.text = comments.rating
            let rating = (comments.rating as NSString).floatValue
            ratingLabel.backgroundColor = CommentModel.color(for: CGFloat(rating))
            commentsTableView.reloadData()
            emptyView.isHidden = allComments().count != 0
        }
        if AppManager.shared.currentUser != nil {
            warningView.isHidden = true
            if comments.userComment != nil {
                addCommentView.isHidden = true
            } else {
                addCommentView.isHidden = false
            }
        } else {
            warningView.isHidden = false
            addCommentView.isHidden = true
        }
    }

    func allComments() -> [CommentModel] {
        if comments == nil {
            return []
        }
        if comments.userComment != nil {
            var temp = comments!.allComments
            temp.insert(comments.userComment!, at: 0)
            return temp
        }
        return comments.allComments
    }
    
    func addComment(for text: String, rating: Double) {
        SVProgressHUD.show()
        CommentsManager.addComment(for: text, rating: rating) { (comment, error) in
            if error != nil {
                self.alert(message: error!)
            }
            if comment != nil {
                self.comments.userComment = comment
                self.prepareView()
            }
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: Actions
    @IBAction func addCommentButtonClicked(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Comments", bundle: nil).instantiateViewController(withIdentifier: "AddCommentViewController") as! AddCommentViewController
        vc.commentClosure = { [weak self] (text, rating) in
            self?.addComment(for: text, rating: rating)
        }
        customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }


}

extension CommentsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allComments().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        cell.configure(for: allComments()[indexPath.row])
        return cell
    }
}
