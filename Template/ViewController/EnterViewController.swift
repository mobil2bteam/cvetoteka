
import UIKit

class EnterViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var recentViewedView: UIView!
    
    // MARK: Properties
    var completionBlock: SignInUserBlock?
    let signUpSegue = "signUpSegue"
    let logInSegue = "logInSegue"
    

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        deliveryView.isHidden = !SettingsManager.showDeliveryCity
        recentViewedView.isHidden = !SettingsManager.showWatchedProducts
    }
    
    // MARK: Actions
    @IBAction func cancelButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == signUpSegue {
            let destination = (segue.destination as! UINavigationController).viewControllers[0] as! SignUpViewController
            destination.completionBlock = completionBlock
        }
        if segue.identifier == logInSegue {
            let destination = (segue.destination as! UINavigationController).viewControllers[0] as! LogInViewController
            destination.completionBlock = completionBlock
        }
    }
}
