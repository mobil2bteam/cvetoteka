
import UIKit
import ObjectMapper
import ImageSlideshow

class MainViewController: BaseViewController {
    
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var recentViewedView: UIView!

    var refreshControl: UIRefreshControl!
    let menuSegue = "embeddedMenuSegue"
    let groupSegue = "embeddedGroupSegue"
    let recentViewedSegue = "embeddedRecentViewedSegue"
    var menuVC: MenuViewController!
    var groupVC: GroupViewController!
    var recentViewedVC: RecentViewedViewController!

    //***************************************************
    // MARK: - Lifecycle -
    //***************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        if Constants.appMode == .tabBar {
            navigationItem.leftBarButtonItem = nil
        }
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(updateHome), for: .valueChanged)
        scrollView.addSubview(refreshControl)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.didTapSlider))
        slideshow.addGestureRecognizer(gestureRecognizer)
    }
    
    func didTapSlider() {
        if let link = home.slider!.items[slideshow.currentPage].link {
            if link.name != nil {
                OptionsManager.request(for: link, fromController: self)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Главная"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        title = ""
    }
    
    func updateHome() {
        OptionsManager.loadOptions(useCache: false).then {
            options in
            AppManager.shared.options = options
            }.catch { [weak self] error in
                self?.alert(message: error.localizedDescription)
            }.always {
                self.prepareView()
                self.refreshControl.endRefreshing()
        }
    }

    //***************************************************
    // MARK: - Preparation view -
    //***************************************************

    func prepareView() {
        prepareSliderView()
        menuVC.menu = home.menu!
        menuVC.menuCollectionView.reloadData()
        
        groupVC.groups = appManager.options.home.filteredGrouprs
        groupVC.groupsTableView.reloadData()
        
        socialView.isHidden = !SettingsManager.showSocialLinks
        deliveryView.isHidden = !SettingsManager.showDeliveryCity
        recentViewedView.isHidden = !SettingsManager.showWatchedProducts
    }

    func prepareSliderView() {
        if let slider = appManager.options.home.slider {
            slideshow.prepareFor(slider: slider, sizeHandler: { [weak self] (multiplier) in
                guard let `self` = self else { return }
                self.slideshow.heightAnchor.constraint(equalTo: self.slideshow.widthAnchor, multiplier: multiplier).isActive = true
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    self.view.layoutIfNeeded()
                }
            }, errorHandler: {
                self.slideshow.isHidden = false
            })
        } else {
            slideshow.isHidden = true
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == menuSegue) {
            let vc = segue.destination as! MenuViewController
            vc.menu = home.menu!
            menuVC = vc
        }
        if (segue.identifier == groupSegue) {
            let vc = segue.destination as! GroupViewController
            vc.groups = home.groups
            groupVC = vc
        }
        if segue.identifier == recentViewedSegue {
            let vc = segue.destination as! RecentViewedViewController
            recentViewedVC = vc
        }
    }
    
}

