

import UIKit

class BaseViewController: HamburgerViewController {
    var appManager = AppManager.shared
    var options: OptionsModel! = AppManager.shared.options
    var home: HomeModel! = AppManager.shared.options.home
    var catalog: CatalogModel! = AppManager.shared.catalog
    var newsList: NewsModel! = AppManager.shared.newsList
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

}
