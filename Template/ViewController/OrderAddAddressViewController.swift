
import UIKit

class OrderAddAddressViewController: UIViewController {

    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var commentTextField: TextField!
    @IBOutlet weak var homeTextField: TextField!
    @IBOutlet weak var flatTextField: TextField!
    @IBOutlet weak var streetTextField: TextField!
    @IBOutlet weak var zipCodeTextField: TextField!
    
    var addressHandler: ((AddressDataModel) -> ())?
    var addresses: [AddressDataModel] = [] {
        didSet {
            addressTableView.reloadData()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.colorTextAccent
        view.backgroundColor = UIColor.colorBackgroundDark
        addresses = OrderDataManager.getAllAdresses()
        addressTableView.separatorColor = UIColor.colorBackgroundDark
        addressTableView.separatorStyle = .singleLine
        addressTableView.backgroundColor = UIColor.colorBackgroundDark
        addressTableView.tableFooterView = UIView.init()
        title = "Адрес доставки"
    }
    
    
    // MARK: Actions
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonClicked(_ sender: Any) {
        if streetTextField.text!.isEmpty {
            streetTextField.shake()
            return
        }
        if homeTextField.text!.isEmpty {
            homeTextField.shake()
            return
        }
        if flatTextField.text!.isEmpty {
            flatTextField.shake()
            return
        }
        if streetTextField.text!.isEmpty {
            streetTextField.shake()
            return
        }
        let street = streetTextField.text!
        let home = homeTextField.text!
        let flat = flatTextField.text!
        let comment = commentTextField.text!
        let zipCode = zipCodeTextField.text!

        let address = OrderDataManager.addAddress(street: street, home: home, flat: flat, comment: comment, zipCode: zipCode)
        dismiss(animated: true, completion: { [weak self] in
            if self?.addressHandler != nil {
                self?.addressHandler!(address)
            }
        })
    }

    
    // MARK: Methods

    func deleteAddress(address: AddressDataModel) {
        customAlert(title: "Удалить адрес?") { [weak self] in
            OrderDataManager.removeAddress(address: address)
            self?.addresses = OrderDataManager.getAllAdresses()
        }
    }
    
}


extension OrderAddAddressViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipientTableViewCell", for: indexPath) as! RecipientTableViewCell
        let address = addresses[indexPath.row]
        cell.configureAddress(for: address)
        cell.deleteHandler = { [weak self] in
            self?.deleteAddress(address: address)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let address = addresses[indexPath.row]
        dismiss(animated: true, completion: { [weak self] in
            if self?.addressHandler != nil {
                self?.addressHandler!(address)
            }
        })
    }
}

extension OrderAddAddressViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == zipCodeTextField {
            _ = streetTextField.becomeFirstResponder()
        }
        if textField == streetTextField {
            _ = homeTextField.becomeFirstResponder()
        }
        if textField == homeTextField {
            _ = flatTextField.becomeFirstResponder()
        }
        if textField == flatTextField {
            _ = commentTextField.becomeFirstResponder()
        }
        _ = textField.resignFirstResponder()
        return true
    }
}
