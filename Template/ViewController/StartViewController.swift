
import UIKit
import SVProgressHUD

class StartViewController: UIViewController {

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    // MARK: Methods
    func loadData() {
        OptionsManager.loadOptions { [weak self] (error) in
            if (error != nil) {
                self?.alert(message: error!.localizedDescription, handler: { (action) in
                    self?.loadData()
                })
            } else {
                self?.prepare()
            }
        }
    }
    
    func prepare() {
        if Constants.appMode == .tabBar {
            let tabBarVC = MainTabBarViewController.init()
            present(tabBarVC, animated: true, completion: nil)
        } else {
            let userDefaults = UserDefaults.standard
            if !userDefaults.bool(forKey: "firstLoad") {
                showOnboardViewController()
                userDefaults.set(true, forKey: "firstLoad")
                userDefaults.synchronize()
            } else {
                showMainViewController()
            }
        }
    }
    
    func showOnboardViewController() {
        let vc = UIStoryboard.init(name: "Onboard", bundle: nil).instantiateInitialViewController()! as! OnboardViewController
        vc.doneCompletion = {
            self.showMainViewController()
        }
        present(vc, animated: false, completion: nil)
    }

    func showMainViewController() {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()!
        UIApplication.shared.delegate!.window!!.rootViewController = vc
    }

}
