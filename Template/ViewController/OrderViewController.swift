

import UIKit
import SVProgressHUD

class OrderViewController: UIViewController {

    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var deliveryPriceLabel: UILabel!
    @IBOutlet weak var productsTableView: UITableView!
    @IBOutlet weak var productsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var paymentView: UIView!
    
    let cartCellHeight = 120
    var orderId: Int!
    var order: OrderModel! {
        didSet {
            prepareView()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Заказ"
        loadOrder()
    }

    // MARK: Load data
    func loadOrder() {
        if orderId != nil {
            SVProgressHUD.show()
            OptionsManager.getOrder(for: orderId, successHandler: { [weak self] (order, error) in
                SVProgressHUD.dismiss()
                if order != nil {
                    self?.order = order
                }
                if error != nil {
                    self?.errorLabel.text = error!
                }
            })
        }
    }
    
    // MARK: Methods
    func prepareView() {
        orderLabel.text = "Заказ №" + String(order.number)
        orderStatusLabel.text = order.status
        dateLabel.text = order.createdAt
        paymentStatusLabel.text = order.isPayment == true ? "Оплачен" : "Не оплачен"
        summLabel.text = "\(order.sumSale) р."
        saleLabel.text = "\(order.totalSale) р."
        totalLabel.text = "\(order.total) р."
        deliveryPriceLabel.text = "\(order.sumDelivery) р."
        countLabel.text = "\(order.products.count) шт."
        paymentLabel.text = order.payment?.paymentText ?? ""
        if let delivery = order.delivery {
            deliveryLabel.text = delivery.deliveryText
            if delivery.deliveryType == .pickup {
                addressLabel.text = delivery.addressPoint
            } else {
                addressLabel.text = order.address
            }
        }
        orderStatusLabel.text = order.status
        orderLabel.text = "Заказ №\(order.number!)"
        productsTableViewHeightConstraint.constant = CGFloat(cartCellHeight * order.products.count)
        view.layoutIfNeeded()
        productsTableView.reloadData()
        paymentView.isHidden = !(order.payment?.paymentType == .cart && !order.isPayment)
        errorView.isHidden = true
    }
    
    // MARK: Actions
    @IBAction func repeatOrderButtonClicked(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Cart", bundle: nil).instantiateInitialViewController()
        let block = {
            self.order.products.filter{ $0.prices.count > 0 }.forEach{
                CartManager.addProduct(for: $0.prices.first!.id, productId: $0.id)
            }
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        if CartManager.isEmpty {
            block()
        } else {
            let title = "Очистить корзину перед тем, как добавить товар из заказа?"
            customAlert(title: title, cancelHandler: { 
                block()
            }, okHandler: { 
                CartManager.clearCart()
                block()
            })
        }
    }

    @IBAction func payButtonClicked(_ sender: Any) {
    
    }
}

extension OrderViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if order != nil {
            return order.products.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cart2TableViewCell", for: indexPath) as! Cart2TableViewCell
        cell.configureOrder(for: order.products[indexPath.row])
        return cell
    }
    
}

