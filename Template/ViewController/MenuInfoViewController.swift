
import UIKit
import SVProgressHUD
import AlamofireImage

class MenuInfoViewController: UIViewController {

    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var descriptionDetailLabel: UILabel!
    @IBOutlet weak var descriptionShortLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionShortView: BackgroundView!
    @IBOutlet weak var imageContentView: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var partnerView: UIView!

    var menuId: Int!
    var menuInfo: MenuInfoModel!
    var isPartnerInfo = false
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        partnerView.isHidden = !isPartnerInfo
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadMenu()
    }
    
    // MARK: Methods
    func loadMenu() {
        if menuId != nil && isPartnerInfo == false {
            SVProgressHUD.show()
            OptionsManager.getMenuInfo(for: menuId, successHandler: { (menuInfo, error) in
                SVProgressHUD.dismiss()
                if menuInfo != nil {
                    self.menuInfo = menuInfo
                    self.prepareView()
                }
                if error != nil {
                    self.errorLabel.text = error
                }
            })
        }
        if isPartnerInfo == true {
            menuInfo = AppManager.shared.options.settings.partnerInfo
            prepareView()
        }
    }
    
    func prepareView() {
        title = menuInfo.name
        if let url = URL(string: menuInfo.url ?? "") {
            webView.loadRequest(URLRequest.init(url: url))
        } else {
            webView.isHidden = true
            if menuInfo.descriptionShort.isEmpty {
                descriptionShortView.isHidden = true
            } else {
                descriptionShortLabel.text = menuInfo.descriptionShort
            }
            descriptionDetailLabel.setHtmlText(menuInfo.descriptionLong)
            if let image = menuInfo.image, let url = URL(string: image.url) {
                imageView.af_setImage(withURL: url)
                let multiplier = image.aspectRatio
                imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: multiplier).isActive = true
                DispatchQueue.main.async { [weak self] in
                    self?.view.layoutIfNeeded()
                }
                imageContentView.isHidden = false
            }
        }
        errorView.isHidden = true
    }
    
    @IBAction func partnerButtonClicked(_ sender: Any) {
        SVProgressHUD.show()
        OptionsManager.partnerFeed(successHandler: { (success, error) in
            SVProgressHUD.dismiss()
            if success != nil {
                if success == true {
                    AppManager.shared.currentUser!.info.partnerFeed = true
                    self.navigationController?.popViewController(animated: true)
                }
            }
            if error != nil {
                self.alert(message: error!)
            }
        })
    }

}
