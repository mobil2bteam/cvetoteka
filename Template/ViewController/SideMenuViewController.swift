
import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet var buttonsArray: [UIButton]!
    @IBOutlet var viewsArray: [UIView]!
    @IBOutlet weak var partnerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonsArray.forEach{
            $0.setTitleColor(UIColor.colorTextNavMenu, for: .normal)
            $0.tintColor = UIColor.colorIconNavMenu
        }
        viewsArray.forEach{$0.backgroundColor = UIColor.colorBackgroundNav}
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        partnerView.isHidden = AppManager.shared.options.settings.partnerInfo == nil
    }
    
    // MARK: Actions
    @IBAction func profileButtonClicked(_ sender: Any) {
        if AppManager.shared.currentUser == nil {
            let vc = UIStoryboard.init(name: "Login", bundle: nil).instantiateInitialViewController()! as! UINavigationController
            (vc.viewControllers[0] as! EnterViewController).completionBlock =  { [weak self] in
                self?.dismiss(animated: false, completion: nil)
                self?.performSegue(withIdentifier: "profileSegue", sender: nil)
            }
            present(vc, animated: false, completion: nil)
        } else {
            performSegue(withIdentifier: "profileSegue", sender: nil)
        }
    }

    @IBAction func partnerButtonClicked(_ sender: Any) {
        let vc = Router.menuInfoViewController(for: 0)
        vc.isPartnerInfo = true
        navigationController?.pushViewController(vc, animated: true)
    }
}
