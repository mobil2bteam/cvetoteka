
import UIKit
import ImageSlideshow

class PointViewController: UIViewController {
    
    @IBOutlet weak var cashView: UIView!
    @IBOutlet weak var freeDeliveryView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var freeDeliveryPriceLabel: UILabel!
    @IBOutlet weak var deliveryPriceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var orderView: UIView!
    @IBOutlet var buttonsArray: [UIButton]!

    var point: PointModel!
    var orderMode = false
    var orderCallback: ((PointModel) -> ())?

    // MARK: Lifecycle
    convenience init(point: PointModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.point = point
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        orderView.isHidden = !orderMode
        buttonsArray.forEach{ $0.tintColor = UIColor.colorPrimary }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = point.name
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    // MARK: Methods
    func prepareView() {
        pointLabel.text = point.name
        addressLabel.text = point.contact.address.street
        descriptionView.isHidden = point.descriptionLong.isEmpty
        cashView.isHidden = point.checkPointCash
        deliveryPriceLabel.text = "\(point.summDelivery!) р."
        if point.summFreeDelivery != nil &&  point.summFreeDelivery > 0 {
            freeDeliveryPriceLabel.text = "\(point.summFreeDelivery!) р."
        } else {
            freeDeliveryView.isHidden = true
        }
        prepareSliderView()
    }
    
    func prepareSliderView() {
        if let slider = point.slider {
            if slider.items.count > 0 {
                let multiplier = slider.items[0].image.aspectRatio
                slideshow.heightAnchor.constraint(equalTo: slideshow.widthAnchor, multiplier: multiplier).isActive = true
                DispatchQueue.main.async { [weak self] in
                    self?.view.layoutIfNeeded()
                }
            }
            if slider.auto {
                slideshow.slideshowInterval = Double(slider.speed / 1000)
            } else {
                slideshow.slideshowInterval = 0
            }
            slideshow.draggingEnabled = slider.manual
            slideshow.pageControlPosition = slider.items.count > 1 ? PageControlPosition.insideScrollView : PageControlPosition.hidden
            slideshow.pageControl.currentPageIndicatorTintColor = UIColor.colorAccent
            slideshow.pageControl.pageIndicatorTintColor = UIColor.white
            slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
            slideshow.setImageInputs(slider.items.map({ (item) -> AlamofireSource in
                return AlamofireSource(urlString: item.image.url)!
            }))
            slideshow.isHidden = false
            
        } else {
            slideshow.isHidden = true
        }
    }

    
    // MARK: Actions
    @IBAction func callButtonClicked(_ sender: Any) {
        let onlyDigits = (point.contact.phone.components(separatedBy: CharacterSet.decimalDigits.inverted)).joined(separator: "")
        let phoneURL = URL.init(string: "telprompt:\(onlyDigits)")
        if phoneURL != nil {
            openURL(urlString: "telprompt:\(onlyDigits)")
        }
    }
    
    @IBAction func orderButtonClicked(_ sender: Any) {
        if AppManager.shared.currentUser != nil {
            dismiss(animated: true, completion: { [weak self] in
                if self?.orderCallback != nil {
                    self?.orderCallback!(self!.point)
                }
            })
        } else {
            let vc = Router.signUpViewController()
            vc.completionBlock = { [weak self] in
                self?.dismiss(animated: false, completion: nil)
                self?.submitOrder()
            }
            let navVC = UINavigationController.init(rootViewController: vc)
            present(navVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func workingHoursButtonClicked(_ sender: Any) {
        let vc = Router.workingViewController(for: point.workingHours)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func descriptionButtonClicked(_ sender: Any) {
        let vc = Router.descriptionViewController(for: point.descriptionLong)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func submitOrder() {
        dismiss(animated: true, completion: { [weak self] in
            if self?.orderCallback != nil {
                self?.orderCallback!(self!.point)
            }
        })
    }

    // MARK: Helper
    func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
