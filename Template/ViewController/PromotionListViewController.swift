
import UIKit
import SVProgressHUD

class PromotionListViewController: UIViewController {
    
    
    // MARK: Outlets
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: EmptyLabel!

    var appManager = AppManager.shared
    
    
    // MARK: Properties
    let identifier = String(describing: GroupTableViewCell.self)

    
    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        emptyView.isHidden = appManager.newsList.items.count != 0
        prepareEmptyView()
        setupCollectionView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        title = "Новости и акции"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    // MARK: Methods
    func prepareEmptyView() {
        emptyLabel.setHtmlText(Constants.emptyNews)
    }

    func setupCollectionView() {
        collectionView.register(NewsCollectionViewCell.nib(), forCellWithReuseIdentifier: identifier)
    }
    

    // MARK: Navigation
    func showPromotionViewController(for newsId: Int) {
        let vc = Router.promotionViewController(for: newsId)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension PromotionListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = appManager.newsList.items[indexPath.item]
        showPromotionViewController(for: item.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appManager.newsList.items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! NewsCollectionViewCell
        let item = appManager.newsList.items[indexPath.item]
        cell.configure(for: item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let imageHorizontalInsets: CGFloat = 8.0 + 8.0
        let imageVerticalInsets: CGFloat = 8.0 + 8.0 + 58.0
        let imageWidth = collectionView.frame.width - imageHorizontalInsets
        guard let image  = appManager.newsList.items[indexPath.item].image, image.isCorrect == true else {
            return CGSize(width: collectionView.frame.width, height: 200)
        }
        let imageHeight = imageWidth * image.aspectRatio
        return CGSize(width: collectionView.frame.width, height: imageHeight + imageVerticalInsets)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat { 
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
}
