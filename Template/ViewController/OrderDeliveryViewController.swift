

import UIKit
import SVProgressHUD

class OrderDeliveryViewController: UIViewController {

    
    // MARK: Outlets
    @IBOutlet weak var deliveryTableView: UITableView!

    
    // MARK: Properties
    var cart: CartModel!
    var params: [String: String]!
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let closeButton = UIBarButtonItem(image: UIImage.init(named: "icon_close"), style: .plain, target: self, action: #selector(closeButtonClicked))
        navigationItem.rightBarButtonItem = closeButton
        NotificationCenter.default.addObserver(self, selector: #selector(OrderDeliveryViewController.prepareCart), name: NSNotification.Name(rawValue: CityManager.notificationName), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Новый заказ"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CityManager.notificationName), object: nil)
    }

    // MARK: Methods
    func prepareCart() {
        SVProgressHUD.show()
        OptionsManager.loadCart(params: params, successHandler: { (cart, error) in
            SVProgressHUD.dismiss()
            if cart != nil {
                self.cart = cart
                self.deliveryTableView.reloadData()
            }
            if error != nil {
                self.alert(message: error!.localizedDescription)
            }
        })
    }
    
    
    // MARK: Actions
    func closeButtonClicked() {
        dismiss(animated: true, completion: nil)
    }
    

}

extension OrderDeliveryViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart.deliveryArray().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryTableViewCell", for: indexPath) as! DeliveryTableViewCell
        let delivery = cart.deliveryArray()[indexPath.row]
        cell.configure(for: delivery)
        if delivery.type == .pickup {
            let minPrice = AppManager.shared.options.settings.company.minPriceForPoints
            let maxPrice = AppManager.shared.options.settings.company.maxPriceForPoints
            if minPrice != maxPrice {
                cell.priceLabel.text =  "\(minPrice)-\(maxPrice) р."
            } else {
                cell.priceLabel.text =  "\(minPrice) р."
            }
            if minPrice == 0 {
                cell.priceLabel.text = "Бесплатно"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let delivery = cart.deliveryArray()[indexPath.row]
        if delivery.type != .pickup {
            let vc = Router.orderDetailViewController(for: delivery)
            vc.params = params
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let points = AppManager.shared.options.settings.company.points()
            let vc = PointListViewController.init(points: points)
            vc.orderMode = true
            vc.orderCallback = { [weak self] point in
                guard let `self` = self else { return }
                let vc = Router.orderSubmitViewController()
                vc.addressPoint = point
                vc.delivery = delivery
                vc.params = self.params
                self.navigationController?.pushViewController(vc, animated: true)
            }
            let navVC = UINavigationController.init(rootViewController: vc)
            present(navVC, animated: true, completion: nil)
        }
    }
}
