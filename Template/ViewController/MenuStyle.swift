
import UIKit

public enum MenuStyle: String {
    case text               = "TEXT"
    case verticalIcon       = "VERTICAL_ICON"
    case verticalImage      = "VERTICAL_IMAGE"
    case horizontalIcon     = "HORIZONTAL_ICON"
    case horizontalImage    = "HORIZONTAL_IMAGE"
    case cellIcon           = "CELL_ICON"
    case cellImage          = "CELL_IMAGE_1"
    
    init(style: String) {
        self = MenuStyle(rawValue: style) ?? .text
    }
}

extension MenuStyle {
    
    var height: CGFloat {
        get {
            switch self {
            case .text:
                return 50
            case .verticalIcon:
                return 60
            case .verticalImage:
                return 120
            case .horizontalIcon:
                return 110
            case .horizontalImage:
                return 100
            case .cellIcon:
                return 100
            case .cellImage:
                return 100
            }
        }
    }
    
    var direction: UICollectionViewScrollDirection {
        get {
            var scrollDirection: UICollectionViewScrollDirection = .horizontal
            switch self {
            case .horizontalIcon:
                scrollDirection = .horizontal
            case .horizontalImage:
                scrollDirection = .horizontal
            default:
                scrollDirection = .vertical
            }
            return scrollDirection
        }
    }
    
    var cellsInRow: CGFloat {
        switch self {
        case .verticalImage:
            return 1.0
        case .horizontalImage:
            return 1.3
        default:
            return 3.0
        }
    }
    
    func cellSize(for width: CGFloat) -> CGSize {
        switch self {
        case .verticalImage:
            let cellWidth = width / cellsInRow
            return CGSize(width: cellWidth, height: height)
        case .horizontalIcon:
            return CGSize(width: 100, height: height)
        case .horizontalImage:
            return CGSize(width: width / 1.3, height: height)
        case .cellIcon:
            let cellWidth = width / cellsInRow
            return CGSize(width: cellWidth, height: height)
        case .cellImage:
            let cellWidth = width / cellsInRow
            return CGSize(width: cellWidth, height: height)
            
        default:
            return CGSize(width: width, height: height)
        }
    }
}
