
import UIKit

class DeliveryViewController: UIViewController {
    
    @IBOutlet weak var deliveryCityLabel: UILabel!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var deliveryButton: UIButton!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = false
        deliveryButton.tintColor = UIColor.colorTextPrimary
        prepareView()
        NotificationCenter.default.addObserver(self, selector: #selector(DeliveryViewController.prepareView), name: NSNotification.Name(rawValue: CityManager.notificationName), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeButton.isHidden = !SettingsManager.allowChangeDeliveryCity
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CityManager.notificationName), object: nil)
    }
    
    // MARK: Methods

    func prepareView() {
        let city = CityManager.getDeliveryCity()
        deliveryCityLabel.text = "Ваш город: \(city.name)"
    }
    
    @IBAction func ttt(_ sender: Any) {
        if SettingsManager.allowChangeDeliveryCity {
            let vc = CityListViewController()
            let navVC = UINavigationController(rootViewController: vc)
            present(navVC, animated: true, completion: nil)
        }
    }
    
}
