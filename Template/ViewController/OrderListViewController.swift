
import UIKit
import SVProgressHUD

class OrderListViewController: HamburgerViewController {

    @IBOutlet weak var ordersTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: EmptyLabel!
    
    var refreshControl: UIRefreshControl!
    var orders: OrdersModel? {
        didSet {
            ordersTableView.reloadData()
            emptyView.isHidden = orders!.allOrders().count != 0
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareEmptyView()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadOrders), for: .valueChanged)
        ordersTableView.addSubview(refreshControl)
        loadOrders()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Мои заказы"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    func prepareEmptyView() {
        emptyLabel.setHtmlText(Constants.emptyOrders)
    }

    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "orderSegue" {
            guard let vc = segue.destination as? OrderViewController else {
                return
            }
            guard let cell = sender as? UITableViewCell else {
                return
            }
            guard let indexPath = ordersTableView.indexPath(for: cell) else {
                return
            }
            let order = orders!.allOrders()[indexPath.row]
            vc.orderId = order.id
        }
    }
    
    // MARK: Load data
    func loadOrders() {
        if AppManager.shared.currentUser == nil {
            refreshControl.endRefreshing()
            return
        }
        refreshControl.beginRefreshing()
        ordersTableView.setContentOffset(CGPoint(x:0 , y:ordersTableView.contentOffset.y - refreshControl.frame.size.height), animated: true)
        OptionsManager.orders { [weak self] (orders, error) in
            SVProgressHUD.dismiss()
            self?.refreshControl.endRefreshing()
            if orders != nil {
                self?.orders = orders
            }
            if error != nil {
                self?.alert(message: error!)
            }
        }
    }

    
}


extension OrderListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders?.allOrders().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell", for: indexPath) as! OrderTableViewCell
        cell.configure(for: orders!.allOrders()[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let order = orders!.allOrders()[indexPath.row]
        print(order.number)
    }
}
