
import UIKit
import ImageSlideshow
import SVProgressHUD
import FTPopOverMenu
import Presentr

class ProductViewController: UIViewController {
    
    var productId: Int!
    var product: ProductModel!
    let cellIdentifier = "PropertyTableViewCell"
    let presenter: Presentr = {
        let customPresenter = Presentr(presentationType: .fullScreen)
        customPresenter.transitionType = .coverVertical
        customPresenter.dismissTransitionType = .coverVertical
        customPresenter.roundCorners = false
        customPresenter.backgroundColor = .black
        customPresenter.backgroundOpacity = 0.5
        customPresenter.dismissOnSwipe = true
        customPresenter.dismissOnTap = true
        return customPresenter
    }()

    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var sliderContainerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionDetailLabel: MainLabel!
    @IBOutlet weak var descriptionDetailView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var cartStackView: UIStackView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var cartLabel: UILabel!
    @IBOutlet weak var pricesTableView: UITableView!
    @IBOutlet weak var pricesTableViewHeightConstraint: NSLayoutConstraint!

    
    // MARK: Lifecycle
    convenience init(productId: Int) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.productId = productId
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        pricesTableView.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        pricesTableView.separatorColor = UIColor.colorBackgroundDark
        loadProduct()
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        if product != nil {
            title = product.name
            prepareCartView()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    // MARK: Load data
    func loadProduct() {
        if productId != nil {
            SVProgressHUD.show()
            OptionsManager.getProduct(for: productId) { [weak self] (product, error) in
                SVProgressHUD.dismiss()
                if product != nil {
                    self?.product = product
                    self?.prepareView()
                    self?.errorView.isHidden = true
                }
                if error != nil {
                    self?.errorLabel.text = error!.localizedDescription
                }
            }
        }
    }

    // MARK: Preparation
    func prepareView() {
        title = product.name
        prepareCartView()
        pricesTableViewHeightConstraint.constant = CGFloat(product.allPrices.count * 50)
        view.layoutIfNeeded()
        pricesTableView.reloadData()
        prepareSliderView()
        if !product.longDescription.isEmpty {
            descriptionDetailLabel.setHtmlText(product.longDescription)
            descriptionDetailView.isHidden = false
        } else {
            descriptionDetailView.isHidden = true
        }
        nameLabel.text = product.name
        descriptionLabel.text = product.shortDescription
        DataManager.addProductToViewedProducts(product: product)
    }
    
    func prepareCartView() {
        if let p = CartManager.productForProductId(productId: product.id) {
            let price = product.priceFor(count: p.count)
            let summ = p.count * price!
            cartLabel.text = "В корзине \(p.count) шт. на сумму: \(summ) pуб."
            priceLabel.text = "\(price!) р."
        } else {
            cartLabel.text = "В корзине 0 шт. на сумму: 0 pуб."
            priceLabel.text = "\(product.allPrices.first?.price ?? 0) р."
        }
    }
    
    func prepareSliderView() {
        let images = product.images
        slideshow.prepareFor(images: images, sizeHandler: { [weak self] (multiplier) in
            guard let `self` = self else { return }
            self.slideshow.heightAnchor.constraint(equalTo: self.slideshow.widthAnchor, multiplier: multiplier).isActive = true
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.view.layoutIfNeeded()
            }
        }) {
            self.sliderContainerView.isHidden = true
        }
    }
    
    func increaseProductFor(price: PriceModel) {
        if let p = CartManager.productForProductId(productId: product.id) {
            if p.priceId == price.id {
                let newCount = p.count + price.minParty
                if newCount > price.maxCount {
                    let priceId = product.priceIdFor(count: newCount)!
                    CartManager.deleteProducts(productId: product.id)
                    CartManager.addProduct(for: priceId, productId: product.id, count: newCount)
                } else {
                    CartManager.updateProduct(priceId: price.id, count: newCount)
                }
            } else {
                CartManager.deleteProducts(productId: product.id)
                CartManager.addProduct(for: price.id, productId: product.id, count: price.minCount)
            }
        } else {
            CartManager.addProduct(for: price.id, productId: product.id, count: price.minCount)
        }
        prepareCartView()
        pricesTableView.reloadData()
    }
    
    // MARK: Actions
    @IBAction func cartButtonClicked(_ sender: Any, event: UIEvent) {
        let vc = PricesViewController.init(product: product)
        vc.completionHandler = { [weak self] price in
            guard let `self` = self else { return }
            self.prepareCartView()
            self.pricesTableView.reloadData()
        }
        customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }

}

extension ProductViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product?.allPrices.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PropertyTableViewCell
        let price = product.allPrices[indexPath.row]
        cell.configure(for: price)
        cell.addToCartHandler = { [weak self] price in
            guard let `self` = self else { return }
            self.increaseProductFor(price: price)
        }
        if let p = CartManager.productForProductId(productId: product.id) {
            if price.minCount <= p.count && price.maxCount >= p.count {
                cell.setDotActive(active: true)
            }
        }
        return cell
    }
    
}

