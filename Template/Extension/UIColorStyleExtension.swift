
import UIKit

extension UIColor {
    
    // Основной цвет приложения
    open class var colorPrimary: UIColor {
        return UIColor("#9ede95")
    }
    
    // Основной темный цвет приложения
    open class var colorPrimaryDark: UIColor {
        return UIColor("#8cc983")
    }
    
    // Основной акцент цвет приложения
    open class var colorAccent: UIColor {
        return UIColor("#ed565b")
    }
    
    // Основной цвет фона
    open class var colorBackground: UIColor {
        return UIColor("#ffffff")
    }
    
    // Основной цвет темного фона, разделительные линии
    open class var colorBackgroundDark: UIColor {
        return UIColor("#f3f3f3")
    }
    
    // Цвет фона заголовка
    open class var colorBackgroundCaptionGroupView: UIColor {
        return UIColor("#ffffff")
    }
    
    // Цвет фона заголовка
    open class var colorBackgroundToolbar: UIColor {
        return UIColor("#ffffff")
    }
    
    // Цвет флага скидки
    open class var colorBackgroundFlagDiscount: UIColor {
        return UIColor("#ED565B")
    }
    
    // Цвет фона основного меню
    open class var colorBackgroundNav: UIColor {
        return UIColor("#f3f3f3")
    }
    
    // Стандартный цвет текста во всем проекте
    open class var colorTextPrimary: UIColor {
        return UIColor("#606277")
    }
    
    // Вспомогательный цвет текста во всем проекте
    open class var colorTextSecond: UIColor {
        return UIColor("#878686")
    }
    
    // Акцент цвет текста во всем проекте
    open class var colorTextAccent: UIColor {
        return UIColor("#8cc983")
    }
    
    // Супер Акцент цвет текста во всем проекте
    open class var colorTextSuperAccent: UIColor {
        return UIColor("#ED565B")
    }
    
    // Белый цвет текста
    open class var colorTextWhite: UIColor {
        return UIColor("#ffffff")
    }
    
    // Цвет текста в пунктах меню
    open class var colorTextNavMenu: UIColor {
        return UIColor("#606277")
    }
    
    // Цвет цены товара
    open class var colorTextPrice: UIColor {
        return UIColor("#606277")
    }
    
    // Цвет цены со скидкой
    open class var colorTextDiscountPrice: UIColor {
        return UIColor("#ed565b")
    }
    
    // Цвет старой цены
    open class var colorTextOldPrice: UIColor {
        return UIColor("#878686")
    }
    
    // Цвет процента скидки на бирке со скидкой
    open class var colorTextFlagDiscount: UIColor {
        return UIColor("#ffffff")
    }
    
    // Цвет текста заголовка на подборке товара
    open class var colorTextCaptionGroupView: UIColor {
        return UIColor("#606277")
    }
    
    // Стандартный цвет текста загловка
    open class var colorTextToolbarTitle: UIColor {
        return UIColor("#606277")
    }
    
    // Стандартный цвет вспомогательного текста загловка
    open class var colorTextToolbarSecond: UIColor {
        return UIColor("#878686")
    }
    
    // Цвет фона баллов, если рейтинг выше 4
    open class var colorTextRating4: UIColor {
        return UIColor("#8CC983")
    }
    
    // Цвет фона баллов, если рейтинг выше 3
    open class var colorTextRating3: UIColor {
        return UIColor("#606277")
    }
    
    // Цвет фона баллов, если рейтинг ниже 3
    open class var colorTextRating2: UIColor {
        return UIColor("#ed565b")
    }
    
    // Стандартный цвет текста на кнопке
    open class var colorButtonTextDefault: UIColor {
        return UIColor("#ffffff")
    }
    
    // Стандартный цвет текста на кнопке типа link
    open class var colorButtonTextPrimaryLink: UIColor {
        return UIColor("#9EDE95")
    }
    
    // Стандартный цвет текста на кнопке типа link, accent стиля
    open class var colorButtonTextAccentLink: UIColor {
        return UIColor("#ED565B")
    }
    
    // Стандартный цвет кнопки
    open class var colorButtonPrimary: UIColor {
        return UIColor("#8CC983")
    }
    
    // Стандартный цвет при нажатии
    open class var colorButtonPrimaryLite: UIColor {
        return UIColor("#8CC983")
    }
    
    // Стандартный цвет кнопки accent
    open class var colorButtonAccent: UIColor {
        return UIColor("#ED565B")
    }
    
    // Стандартный цвет кнопки accent при нажатии
    open class var colorButtonAccentLite: UIColor {
        return UIColor("#ED565B")
    }
    
    // Стандартный цвет всех иконок в приложении
    open class var colorIconPrimary: UIColor {
        return UIColor("#606277")
    }
    
    // Стандартный цвет выделенной иконки
    open class var colorIconSelect: UIColor {
        return UIColor("#FFFFFF")
    }
    
    // Стандартный акцент цвет иконки
    open class var colorIconAccent: UIColor {
        return UIColor("#8CC983")
    }
    
    open class var colorIconSecond: UIColor {
        return UIColor("#828282")
    }
    
    // Стандартный цвет иконок в меню навигации
    open class var colorIconNavMenu: UIColor {
        return UIColor("#606277")
    }
    
}
