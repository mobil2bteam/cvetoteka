
enum ApplicationMode {
    case sideMenu
    case tabBar
}

struct Constants {
    static let apiKey = "000000"
    static let vkIdentifier = "6040974"

    static let apiBaseUrl = "http://main.mobil2b.com/api"
    static let appMode: ApplicationMode = .sideMenu
    
    static let emptyBag = "Ваша корзина пуста"
    static let emptyProducts = "К сожалению поиск не дал результат.\nПопробуйте изменить параметры поиска</string>"
    static let emptyOrders = "Список ваших заказов пуст"
    static let emptyFavoriteProducts = "Список избранных товаров пуст"
    static let emptyNews = "У нас пока нет новостей для вас"
    static let emptyComment = "Нет ни одного отзыва.\nВы можете быть первыми!</string>"

}
