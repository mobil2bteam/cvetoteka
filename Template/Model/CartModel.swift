
import ObjectMapper

public enum DeliveryType: String {
    
    case post               = "post"
    case courier            = "courier"
    case pickup             = "pickup"
    
    init(type: String) {
        self = DeliveryType(rawValue: type) ?? .post
    }
    
}

public enum PaymentType: String {
    case cash               = "cash"
    case cart               = "none-cash"
    
    init(type: String) {
        self = PaymentType(rawValue: type) ?? .cash
    }
    
}

class CartModel: Mappable {

    var products: [CatalogModel] = []
    var promoCode: PromoCodeModel?
    var deliveries: [DeliveryModel] = []
    var totalNotSale: Int = 0
    var sumSale: Int = 0
    var percentSale: Int = 0
    var totalSale: Int = 0
    var totalSumSale: Int = 0
    var totalSumPercent: Int = 0
    var totalCart: Int = 0
    var sumDelivery: Int = 0
    var total: Int = 0

    func deliveryArray() -> [DeliveryModel] {
        var temp = [DeliveryModel]()
        deliveries.forEach { (delivery) in
            if delivery.type != .pickup {
                temp.append(delivery)
            } else {
                let city = CityManager.getDeliveryCity().name
                let points = AppManager.shared.options.settings.company.points(for: city)
                if points.count > 0 {
                    temp.append(delivery)
                }
            }
        }
        return temp
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        products                    <- map["products"]
        totalNotSale                <- map["totalNotSale"]
        sumSale                     <- map["sumSale"]
        percentSale                 <- map["percentSale"]
        totalSale                   <- map["totalSale"]
        totalSumSale                <- map["totalSumSale"]
        totalSumPercent             <- map["totalSumPercent"]
        totalCart                   <- map["totalCart"]
        sumDelivery                 <- map["sumDelivery"]
        total                       <- map["total"]
        deliveries                  <- map["delivery"]
        promoCode                   <- map["promoCode"]
    }

}


class DeliveryModel: Mappable {
    
    var id: Int!
    var price: Int = 0
    var minPrice: Int = 0
    var addText: String = ""
    var name: String!
    var type: DeliveryType!
    var payments: [PaymentModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                              <- map["id"]
        price                           <- map["price"]
        minPrice                        <- map["min_price"]
        addText                         <- map["add_text"]
        name                            <- map["name"]
        type                            <- map["type"]
        payments                        <- map["payment_list.items"]
    }
    
}

class PaymentModel: Mappable {
    
    var id: Int!
    var sort: Int = 0
    var code: String = ""
    var name: String!
    var type: PaymentType!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                              <- map["id"]
        sort                            <- map["sort"]
        code                            <- map["code"]
        name                            <- map["name"]
        type                            <- map["type"]
    }
    
}


class PromoCodeModel: Mappable {
    
    var id: Int!
    var percent: Int = 0
    var totalPromoCode: Int = 0
    var code: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                              <- map["id"]
        percent                         <- map["percent"]
        code                            <- map["code"]
        totalPromoCode                  <- map["totalPromoCode"]
    }
    
}

