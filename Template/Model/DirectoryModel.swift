
import ObjectMapper

class DirectoryModel: Mappable {
    
    var countries: [CountryModel]!
    var cities: [CityModel]!
    var brands: [BrandModel]!
    var sizes: [SizeModel]!
    var colors: [ColorModel]!
    var materials: [MaterialModel]!
    var seasons: [SeasonModel]!
    var ages: [AgeModel]!
    var flags: [FlagModel]!
    var sexes: [SexModel]!
    var manufactures: [ManufactureModel]!
    var sorts: [SortModel]!
    var catalog: CatalogModel!
    var properties: [CatalogModel]!

    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        countries       <- map["country"]
        cities          <- map["city"]
        brands          <- map["brand"]
        sizes           <- map["size"]
        colors          <- map["color"]
        materials       <- map["material"]
        seasons         <- map["season"]
        ages            <- map["age"]
        flags           <- map["flags"]
        sexes           <- map["sex"]
        manufactures    <- map["country_manufacture"]
        sorts           <- map["sort"]
        catalog         <- map["catalog"]
        properties      <- map["properties"]
    }
}

class BaseModel: NSObject, Mappable {
    var id: Int = 0
    var name: String = ""
    var checked = false
    var tempChecked = false

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        name        <- map["name"]
    }
}

class CityModel: BaseModel {
    var countryId: Int!
    var metro: Bool!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        countryId   <- map["country_id"]
        metro       <- map["metro"]
    }
}

class BrandModel: BaseModel {
    var image: String!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id          <- map["id"]
        image       <- map["image"]
        name        <- map["name"]
    }
}

class ColorModel: BaseModel {
    var value: String!
    var colorFull: Bool!

    override func mapping(map: Map) {
        super.mapping(map: map)
        name        <- map["name"]
        colorFull   <- map["colorFull"]
    }
}

class FlagModel: BaseModel {
    var color: String!
    var colorText: String!

    override func mapping(map: Map) {
        super.mapping(map: map)
        color       <- map["color"]
        colorText   <- map["text_color"]
    }
}

// FIXME: model need an implementation

class PropertyModel: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
    }
}

class CountryModel: BaseModel {
    
}

class SizeModel: BaseModel {
    
}

class MaterialModel: BaseModel {
    
}

class SeasonModel: BaseModel {
    
}

class AgeModel: BaseModel {
    
}

class SexModel: BaseModel {
    
}

class ManufactureModel: BaseModel {
    
}

class SortModel: BaseModel {
    
}

