
import ObjectMapper

protocol Product {
    var id: Int! {get set}
    var name: String {get set}
    var prices: [PriceModel] {get set}
}

class ProductModel: Mappable, Product {

    var id: Int!
    var code: String = ""
    var article: String = ""
    var url: String = ""
    var images: [ImageModel] = []
    var name: String = ""
    var flags: [FlagModel] = []
    var properties: [ProductPropertyModel] = []
    var rating: Int = 0
    var commentCount: Int!
    var prices: [PriceModel] = []
    var shortDescription: String = ""
    var longDescription: String = ""
    
    var allPrices: [PriceModel] {
        get {
            return prices.filter{$0.stock}
        }
    }
    
    var allValues: [Int] {
        get {
            if allPrices.count == 0 {
                return [Int]()
            }
            let minCount = allPrices.first!.minCount
            let maxCount = allPrices.last!.minCount! + 1000
            var temp = [Int]()
            for i in minCount! ... maxCount {
                temp.append(i)
            }
            return temp
        }
    }
    
    func priceFor(count: Int) -> Int? {
        return allPrices.filter{$0.minCount <= count && count <= $0.maxCount}.first?.price ?? nil
    }
    
    func priceIdFor(count: Int) -> Int? {
        return allPrices.filter{$0.minCount <= count && count <= $0.maxCount}.first?.id ?? nil
    }

    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        prices                  <- map["prices"]
        commentCount            <- map["commentCount"]
        rating                  <- map["rating"]
        flags                   <- map["flags"]
        images                  <- map["images"]
        code                    <- map["code"]
        article                 <- map["article"]
        properties              <- map["properties"]
        url                     <- map["url"]
        shortDescription        <- map["short_description"]
        longDescription         <- map["description"]
    }

}

class ProductPropertyModel: Mappable {
    
    var id: Int!
    var name: String = ""
    var value: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        value                   <- map["value"]
    }
    
}
