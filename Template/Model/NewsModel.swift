
import ObjectMapper

class NewsModel: Mappable {
    var title: String?
    var view: String?
    var items: [NewsItemModel] = []
    
    required init?(map: Map) {
        
    }
        
    func mapping(map: Map) {
        title               <- map["group_news.title"]
        view                <- map["group_news.view"]
        items               <- map["group_news.items"]
    }
}

class NewsItemModel: Mappable {
    var id:Int!
    var date: String = ""
    var dateFrom: String = ""
    var dateTo: String = ""
    var url: String = ""
    var imageBig: ImageModel?
    var promo = false
    var name: String!
    var addText: String = ""
    var text: String = ""
    var image: ImageModel?
    
    func formattedDate() -> String {
        var formattedDate: String = ""
        if dateTo.isEmpty && dateFrom.isEmpty {
            return formattedDate
        }
        if !dateFrom.isEmpty {
            formattedDate += "с \(dateFrom)"
        }
        if !dateTo.isEmpty {
            formattedDate += " до \(dateTo)"
        }
        return formattedDate
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        date                <- map["date"]
        dateFrom            <- map["dateFrom"]
        dateTo              <- map["dateTo"]
        url                 <- map["url"]
        imageBig            <- map["image_big"]
        promo               <- map["promo"]
        name                <- map["name"]
        addText             <- map["add_text"]
        text                <- map["text"]
        image               <- map["image"]
    }
}
