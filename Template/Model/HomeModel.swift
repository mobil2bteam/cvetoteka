
import ObjectMapper

class HomeModel: Mappable {
    var showCityDelivery: Bool!         // показывать город доставки
    var showSocialLinks: Bool!          // показывать ссылки на соц. сети
    var showWatchedProducts: Bool!      // показывать недавно просмотренные товары
    var showSearchedProducts: Bool!     // показывать недавно искомые товары
    var slider: SliderModel?
    var menu: MenuModel?
    var groups: [GroupModel] = []
    var filteredGrouprs: [GroupModel] {
        get {
            return groups.filter{$0.items.count > 0}
        }
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        showCityDelivery            <- map["flag_show_city_delivery"]
        showSocialLinks             <- map["flag_show_social_link"]
        showWatchedProducts         <- map["flag_show_watched"]
        showSearchedProducts        <- map["flag_show_search"]
        slider                      <- map["slider"]
        menu                        <- map["menu"]
        groups                      <- map["group_view"]
    }
}

class SliderModel: Mappable {
    var id: Int!
    var speed: Int!                 // интервал
    var status: Bool!               // показывать или нет
    var auto: Bool!                 // автоскролл
    var manual: Bool!               // разрешать ручное перелистывание
    var items: [SliderItemModel]!
    
    var correctedImages: [ImageModel] {
        get {
            return items.filter{ $0.image.isCorrect}.map{$0.image}
        }
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id              <- map["id"]
        speed           <- map["speed"]
        status          <- map["status"]
        auto            <- map["auto"]
        manual          <- map["manual"]
        items           <- map["items"]
    }
}

class SliderItemModel: Mappable {
    var image: ImageModel!
    var link: LinkModel?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        image        <- map["image"]
        link         <- map["link"]
    }
}

class ImageModel: Mappable {
    var url: String!
    var width: Int!
    var height: Int!
    var aspectRatio: CGFloat {
        get {
            return CGFloat(height) / CGFloat(width)
        }
    }
    var isCorrect: Bool {
        get {
            return width != 0 && height != 0
        }
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        url             <- map["url"]
        width           <- map["width"]
        height          <- map["height"]
    }
    
}

class MenuModel: NSObject, Mappable {
    var style: MenuStyle = .text
    var items: [MenuItemModel] = []

    required init?(map: Map) {
        
    }
    
    override init() {
        super.init()
    }
        
    func mapping(map: Map) {
        style        <- map["view"]
        items       <- map["items"]
    }
}

class MenuItemModel: Mappable {
    var id: Int!
    var image: ImageModel!
    var icon: ImageModel!
    var name: String!
    var backgroundColor: String!
    var textColor: String!
    var link: LinkModel?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        image                   <- map["image"]
        icon                    <- map["icon"]
        name                    <- map["name"]
        backgroundColor         <- map["background_color"]
        textColor               <- map["text_color"]
        link                    <- map["link"]
    }
}

class LinkModel: Mappable {
    var name: String!
    var params: [ParameterModel]!
    
    func parameters() -> [String: String] {
        var temp = ["key": Constants.apiKey]
        if let token = UserDefaults.standard.string(forKey: "token") {
            temp["token"] = token
        }
        for p in params {
            temp["\(p.name)"] = p.value
        }
        return temp
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name        <- map["name"]
        params      <- map["params"]
    }
}

class ParameterModel: Mappable {
    var name: String = ""
    var value: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name         <- map["name"]
        value        <- map["value"]
    }
}

public enum GroupItemsScrollDirection: String {

    case vertical               = "VERTICAL"
    case horizontal            = "HORIZONTAL"
    
    init(type: String) {
        self = GroupItemsScrollDirection(rawValue: type) ?? .horizontal
    }
    
}

class GroupModel: Mappable {
    var id: Int!
    var itemsCount: Int!
    var itemsDisplay: CGFloat!
    var moreText: String!
    var showButtonMore: Bool = false
    var description: String!
    var name: String!
    var scrollDirection: GroupItemsScrollDirection!
    var link: LinkModel?
    var items: [CatalogModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        itemsCount              <- map["count_items"]
        itemsDisplay            <- map["count_items_display"]
        moreText                <- map["text_more"]
        showButtonMore          <- map["button_more"]
        description             <- map["description"]
        link                    <- map["link"]
        items                   <- map["items"]
        scrollDirection         <- map["view"]
    }
}

