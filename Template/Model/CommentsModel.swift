
import ObjectMapper

class CommentsModel: Mappable {
    
    var name: String!
    var rating: String!
    var userComment: CommentModel?
    var allComments: [CommentModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name                        <- map["name"]
        rating                      <- map["rating"]
        allComments                 <- map["all_comments"]
        userComment                 <- map["user_comment"]
    }
    
}

class CommentModel: Mappable {
    
    var date: String!
    var text: String!
    var rating: String!
    var id: Int!
    var user: UserModel!
    
    required init?(map: Map) {
        
    }
    
    class func color(for rating: CGFloat) -> UIColor {
        if rating > 4.0 {
            return UIColor.colorTextRating4
        }
        if rating > 3.0 {
            return UIColor.colorTextRating3
        }
        return UIColor.colorTextRating2
    }
    
    func mapping(map: Map) {
        date                        <- map["date"]
        text                        <- map["text"]
        rating                      <- map["rating"]
        user                        <- map["user"]
        id                          <- map["id"]
    }
    
}
