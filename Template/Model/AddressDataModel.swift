
import RealmSwift

class AddressDataModel: Object{
    
    dynamic var id: Int = 0
    dynamic var date: Date = Date()
    dynamic var street: String = ""
    dynamic var zipCode: String = ""
    dynamic var home: String = ""
    dynamic var flat: String = ""
    dynamic var comment: String = ""
    
    func formattedAddress() -> String {
        var temp = [street, home, flat]
        if !zipCode.isEmpty {
            temp.insert(zipCode, at: 0)
        }
        return temp.joined(separator: ", ")
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}



