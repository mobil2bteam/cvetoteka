
import RealmSwift

class CartProductDataModel: Object{
    
    dynamic var productId: Int = 0
    dynamic var priceId: Int = 0
    dynamic var count: Int = 0

    override static func primaryKey() -> String? {
        return "priceId"
    }
}



