

import Foundation
import RealmSwift

class CartManager {

    static let notificationName = Notification.Name("CartManagerNotificationIdentifier")
    
    static var isEmpty: Bool {
        get {
            return productsCount() == 0
        }
    }
    
    class func addProduct(for priceId: Int, productId: Int, count: Int = 1) {
        if let existProduct = product(for: priceId) {
            try! uiRealm.write{
                existProduct.count += count
            }
        } else {
            let newProduct = CartProductDataModel()
            newProduct.priceId = priceId
            newProduct.count = count
            newProduct.productId = productId
            try! uiRealm.write{
                uiRealm.add(newProduct)
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func increaseProduct(priceId: Int) {
        if let existProduct = product(for: priceId) {
            try! uiRealm.write{
                existProduct.count += 1
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }

    class func decreaseProduct(priceId: Int) {
        if let existProduct = product(for: priceId) {
            try! uiRealm.write{
                if existProduct.count > 1 {
                    existProduct.count -= 1
                } else {
                    uiRealm.delete(existProduct)
                }
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func updateProduct(priceId: Int, count: Int) {
        if let existProduct = product(for: priceId) {
            try! uiRealm.write{
                existProduct.count = count
                if existProduct.count == 0 {
                    uiRealm.delete(existProduct)
                }
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func deleteProduct(product: CartProductDataModel) {
        try! uiRealm.write{
            uiRealm.delete(product)
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }

    class func deleteProduct(priceId: Int) {
        if let existProduct = product(for: priceId) {
            try! uiRealm.write{
                uiRealm.delete(existProduct)
            }
            NotificationCenter.default.post(name: notificationName, object: nil)
        }
    }

    class func deleteProducts(productId: Int) {
        let products = uiRealm.objects(CartProductDataModel.self).filter{$0.productId == productId}
        if products.count > 0 {
            try! uiRealm.write{
                uiRealm.delete(products)
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func clearCart() {
        let products = uiRealm.objects(CartProductDataModel.self)
        if products.count > 0 {
            try! uiRealm.write{
                uiRealm.delete(products)
            }
        }
    }
    
    class func productsCount() -> Int {
        return uiRealm.objects(CartProductDataModel.self).count
    }

    class func cartParams() -> [String: String] {
        var params = [String: String]()
        params["cart"] = uiRealm.objects(CartProductDataModel.self).map{ "\($0.priceId)-\($0.count)"}.joined(separator: ";")
        return params
    }
    
    class func count(for priceId: Int) -> Int {
        if let existProduct = product(for: priceId) {
            return existProduct.count
        }
        return 0
    }
    
    class func product(for priceId: Int) -> CartProductDataModel? {
        return uiRealm.object(ofType: CartProductDataModel.self, forPrimaryKey: priceId)
    }
    
    class func productForProductId(productId: Int) -> CartProductDataModel? {
        return uiRealm.objects(CartProductDataModel.self).filter{$0.productId == productId}.first
    }
}
