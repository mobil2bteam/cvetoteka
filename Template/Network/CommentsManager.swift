
import Alamofire
import ObjectMapper

class CommentsManager {
    
    static let appManager = AppManager.shared
    
    class func getComments(successHandler: @escaping (CommentsModel?, String?) -> Void) {
        let r = ServerAPI.getComments
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        if let commentsJSON = JSON?["comments"] as? [String: Any] {
                            let model = Mapper<CommentsModel>().map(JSON: commentsJSON)!
                            successHandler(model, nil)
                        }
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    class func addComment(for text: String, rating: Double, successHandler: @escaping (CommentModel?, String?) -> Void) {
        let r = ServerAPI.addComment(rating: rating, text: text)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "test")
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        if let commentJSON = JSON?["comment"] as? [String: Any] {
                            let model = Mapper<CommentModel>().map(JSON: commentJSON)!
                            successHandler(model, nil)
                        }
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

}



