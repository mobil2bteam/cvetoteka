
import Alamofire
import ObjectMapper
import PromiseKit


class OptionsManager {
    
    static let appManager = AppManager.shared
    
    class func loadOptions(useCache: Bool = true, completionBlock: @escaping (Error?) -> Void) {
        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 120
        firstly {
            loadOptions(useCache: useCache)
        }.then { options in
            appManager.options = options
        }.then {
            loadNewsList(useCache: useCache)
        }.then { news in
            appManager.newsList = news
        }.then {
            completionBlock(nil)
        }.catch { error in
            completionBlock(error)
        }
    }

    class func loadOptions(useCache: Bool = true) -> Promise<OptionsModel> {
        return Promise { fulfill, reject in
            let r = ServerAPI.options
            if useCache {
                if let existCache = CacheManager.cache(for: r.path, parameters: r.parameters as? [String : String]) {
                    let optionsJSON = existCache.response.convertToDictionary()!
                    let model = Mapper<OptionsModel>().map(JSON: optionsJSON)!
                    if let userResponse = optionsJSON["user"] as? [String: Any] {
                        let user = Mapper<UserModel>().map(JSON: userResponse)!
                        appManager.currentUser = user
                    }
                    fulfill(model)
                    return
                }
            }
            Alamofire.request(r.path, method: r.method, parameters: r.parameters)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let JSON = response.result.value as? [String: Any]
                        print(JSON ?? "")
                        let model = Mapper<OptionsModel>().map(JSON: JSON!)!
                        if let userResponse = JSON?["user"] as? [String: Any] {
                            print(userResponse)
                            let user = Mapper<UserModel>().map(JSON: userResponse)!
                            appManager.currentUser = user
                        }
                        // Cache
                        if let cacheTime = JSON?["cache"] as? Int {
                            let cacheResponse = CacheRealmModel(url: r.path, response: JSON!, seconds: cacheTime, parameters: r.parameters as? Dictionary<String, String>)
                            CacheManager.addCache(cache: cacheResponse)
                        }
                        fulfill(model)
                    case .failure(let error):
                        reject(error)
                    }
            }
        }
    }
    
    private class func loadNewsList(useCache: Bool = true) -> Promise<NewsModel> {
        return Promise { fulfill, reject in
            let r = ServerAPI.newsList
            if useCache {
                if let existCache = CacheManager.cache(for: r.path, parameters: r.parameters as? [String : String]) {
                    if useCache {
                        let JSON = existCache.response.convertToDictionary()!
                        let model = Mapper<NewsModel>().map(JSON: JSON)!
                        fulfill(model)
                        return
                    }
                }
            }
            Alamofire.request(r.path, method: r.method, parameters: r.parameters)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let JSON = response.result.value as? [String: Any]
                        let model = Mapper<NewsModel>().map(JSON: JSON!)!
                        // Cache
                        if let cacheTime = JSON?["cache"] as? Int {
                            let cacheResponse = CacheRealmModel(url: r.path, response: JSON!, seconds: cacheTime, parameters: r.parameters as? Dictionary<String, String>)
                            CacheManager.addCache(cache: cacheResponse)
                        }
                        fulfill(model)
                    case .failure(let error):
                        reject(error)
                    }
            }
        }
    }

    class func loadProducts(params: [String: String] = [:]) -> Promise<ProductsModel> {
        return Promise { fulfill, reject in
            let r = ServerAPI.products(params: params)
            Alamofire.request(r.path, method: r.method, parameters: r.parameters)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let JSON = response.result.value as? [String: Any]
                        let model = Mapper<ProductsModel>().map(JSON: JSON!)!
                        fulfill(model)
                    case .failure(let error):
                        reject(error)
                    }
            }
        }
    }
    
    class func loadCart(params: [String: String], successHandler: @escaping (CartModel?, Error?) -> Void) {
        let r = ServerAPI.cart(params: params)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                    let model = Mapper<CartModel>().map(JSON: JSON!["cart"] as! [String : Any])!
                    successHandler(model, nil)
                case .failure(let error):
                    successHandler(nil, error)
                }
        }
    }

    class func order(params: [String: String], successHandler: @escaping (OrderModel?, String?) -> Void) {
        let r = ServerAPI.order(params: params)
        print(r.parameters)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        let model = Mapper<OrderModel>().map(JSON: JSON!["order"] as! [String : Any])!
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

    class func getOrder(for id: Int, successHandler: @escaping (OrderModel?, String?) -> Void) {
        let r = ServerAPI.getOrder(orderId: id)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        let model = Mapper<OrderModel>().map(JSON: JSON!["order"] as! [String : Any])!
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    class func updateOrder(for id: Int, isPayment: Bool, successHandler: @escaping (OrderModel?, String?) -> Void) {
        let r = ServerAPI.updateOrder(orderId: id, isPayment: isPayment)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        let model = Mapper<OrderModel>().map(JSON: JSON!["order"] as! [String : Any])!
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

    class func orders(successHandler: @escaping (OrdersModel?, String?) -> Void) {
        let r = ServerAPI.orders
        print(r.parameters)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        print(JSON ?? "")
                        let model = Mapper<OrdersModel>().map(JSON: JSON!["orders"] as! [String : Any])!
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    
    class func loadHome(useCache: Bool = true) -> Promise<HomeModel> {
        let r = ServerAPI.home
        return Promise { fulfill, reject in
            Alamofire.request(r.path, method: r.method, parameters: r.parameters)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let JSON = response.result.value as? [String: Any]
                      //  cacheIfNeeded(JSON!, url: r.path)
                        let model = Mapper<HomeModel>().map(JSON: JSON!)!
                        fulfill(model)
                    case .failure(let error):
                        reject(error)
                    }
            }
        }
    }
    
    
    class func loadCatalog(catalogID: Int? = nil, useCache: Bool = true) -> Promise<CatalogModel> {
        let r = ServerAPI.catalog(parentID: catalogID)
        return Promise { fulfill, reject in
            if useCache {
                if let existCache = CacheManager.cache(for: r.path, parameters: r.parameters as? [String : String]) {
                    if useCache {
                        let JSON = existCache.response.convertToDictionary()!
                        let model = Mapper<CatalogModel>().map(JSON: JSON)!
                        fulfill(model)
                        return
                    }
                }
            }
            Alamofire.request(r.path, method: r.method, parameters: r.parameters)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        let JSON = response.result.value as? [String: Any]
                        let model = Mapper<CatalogModel>().map(JSON: JSON!)!
                        // Cache
                        if let cacheTime = JSON?["cache"] as? Int {
                            let cacheResponse = CacheRealmModel(url: r.path, response: JSON!, seconds: cacheTime, parameters: r.parameters as? Dictionary<String, String>)
                            CacheManager.addCache(cache: cacheResponse)
                        }
                        fulfill(model)
                    case .failure(let error):
                        reject(error)
                    }
            }
        }
    }
    
    class func getNewsItem(newsId: Int, successHandler: @escaping (NewsItemModel?, Error?) -> Void) {
        let r = ServerAPI.news(newsId: newsId)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    //cacheIfNeeded(JSON!, url: r.path)
                    let model = Mapper<NewsItemModel>().map(JSON: JSON!["news_item"] as! [String : Any])!
                    successHandler(model, nil)
                case .failure(let error):
                    successHandler(nil, error)
                }
        }
    }
    
    class func getProduct(for productId: Int, successHandler: @escaping (ProductModel?, Error?) -> Void) {
        let r = ServerAPI.product(productId: productId)
        print(r.parameters)
        print(r.path)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                   // cacheIfNeeded(JSON!, url: r.path)
                    let model = Mapper<ProductModel>().map(JSON: JSON!["product"] as! [String : Any])!
                    successHandler(model, nil)
                case .failure(let error):
                    successHandler(nil, error)
                }
        }
    }

    class func getMenuInfo(for menuId: Int, successHandler: @escaping (MenuInfoModel?, String?) -> Void) {
        let r = ServerAPI.menu(menuId: menuId)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        let model = Mapper<MenuInfoModel>().map(JSON: JSON!["menu_info"] as! [String : Any])!
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

    class func partnerFeed(successHandler: @escaping (Bool?, String?) -> Void) {
        let r = ServerAPI.partner
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        var success = false
                        if let successResponse = JSON?["success"] as? [String: Any] {
                            if let result = successResponse["status"] as? Bool {
                                success = result
                            }
                        }
                        successHandler(success, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

    class func request(for link: LinkModel, fromController: UIViewController) {
        guard let url = appManager.options.url(for: link.name) else {
            return
        }
        let method = HTTPMethod.init(rawValue: url.method) ?? .post
        let path = url.url ?? ""
        let params = link.parameters()
        Alamofire.request(path, method: method, parameters: params)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        fromController.alert(message: error)
                    }
                    if JSON?["products"] != nil {
                        let model = Mapper<ProductsModel>().map(JSON: JSON!)!
                        let vc = Router.getProductsViewController()
                        vc.products = model
                        fromController.navigationController?.pushViewController(vc, animated: true)
                    }
                    if JSON?["catalog"] != nil {
                        let model = Mapper<CatalogModel>().map(JSON: JSON!["catalog"] as! [String : Any])!
                        if model.showSubcatalogOnClick {
                            showCatalog(fromController: fromController, catalog: model)
                        } else {
                            showProducts(fromController: fromController, for: model.id)
                        }
                    }
                    if JSON?["news_item"] != nil {
                        let model = Mapper<NewsItemModel>().map(JSON: JSON!["news_item"] as! [String : Any])!
                        let vc = Router.promotionViewController(for: model.id)
                        fromController.navigationController?.pushViewController(vc, animated: true)
                    }

                case .failure(let error):
                    fromController.alert(message: error.localizedDescription)
                }
        }
    }
    
    // MARK: - Navigation
    private class func showProducts (fromController: UIViewController, for catalogId: Int) {
        let vc = Router.productsViewController(for: catalogId)
        fromController.navigationController?.pushViewController(vc, animated: true)
    }
    
    private class func showCatalog (fromController: UIViewController, catalog: CatalogModel) {
        let vc = Router.catalogViewController(catalog: catalog)
        fromController.navigationController?.pushViewController(vc, animated: true)
    }

}



