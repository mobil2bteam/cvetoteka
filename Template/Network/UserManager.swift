
import Alamofire
import ObjectMapper

class UserManager {
    
    static let appManager = AppManager.shared
    
    class func registerUser(with params: [String: String], successHandler: @escaping (UserModel?, String?) -> Void) {
        let r = ServerAPI.signUp(params: params)
        print(r.parameters)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    }
                    if let userResponse = JSON?["user"] as? [String: Any] {
                        let model = Mapper<UserModel>().map(JSON: userResponse)!
                        saveUser(user: model)
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    class func login(with params: [String: String], successHandler: @escaping (UserModel?, String?) -> Void) {
        let r = ServerAPI.logIn(params: params)
        print(r.parameters)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    print(JSON ?? "")
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        print(error)
                        successHandler(nil, error)
                    }
                    if let userResponse = JSON?["user"] as? [String: Any] {
                        let model = Mapper<UserModel>().map(JSON: userResponse)!
                        saveUser(user: model)
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    
    class func updateUser(with params: [String: String], successHandler: @escaping (UserModel?, String?) -> Void) {
        let r = ServerAPI.updateUser(params: params)
        print(r.parameters)
        print(r.path)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    }
                    if let userResponse = JSON?["user"] as? [String: Any] {
                        print(userResponse)
                        let model = Mapper<UserModel>().map(JSON: userResponse)!
                        saveUser(user: model)
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

    class func changePassword(with params: [String: String], successHandler: @escaping (UserModel?, String?) -> Void) {
        let r = ServerAPI.changePassword(params: params)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    }
                    if let userResponse = JSON?["user"] as? [String: Any] {
                        let model = Mapper<UserModel>().map(JSON: userResponse)!
                        saveUser(user: model)
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

    class func restorePassword(for email: String, successHandler: @escaping (String?, String?) -> Void) {
        let r = ServerAPI.restorePassword(email: email)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    }
                    if let messageResponse = JSON?["message"] as? [String: Any] {
                        let message = messageResponse["text"] as! String
                        successHandler(message, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

    private class func saveUser(user: UserModel) {
        AppManager.shared.currentUser = user
    }
}



